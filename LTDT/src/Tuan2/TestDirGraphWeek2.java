﻿package Tuan2;

public class TestDirGraphWeek2 {
	public static void main(String[] args) {
		System.out.println("Exe Week 2");
		DirGraph dg10 = new DirGraph(7);
		dg10.addEdges(1, 2);
		dg10.addEdges(3, 2);
		dg10.addEdges(3, 4);
		dg10.addEdges(5, 4);
		dg10.addEdges(5,6);
                                 dg10.addEdges(7,6);
		dg10.printGraph();
		System.out.println("Duyệt DFS");
		dg10.re_DFS(3);

		System.out.println();
		System.out.println("Kiem Tra Lien Thong");
		System.out.println(dg10.isConnection());
		System.out.println();
		System.out.println("So Thanh Phan Lien Thong");
		System.out.println(dg10.countConnection());
		System.out.println();
		System.out.println("Tim Thanh Phan Lien Thong Cua Do Thi");
		dg10.findConnection();

	}
}
