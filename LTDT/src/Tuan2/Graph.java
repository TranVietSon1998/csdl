package Tuan2;

import java.util.ArrayList;

public abstract class Graph {
	public int numVex;
	public int[][] adj;
	public boolean[] visited;
	protected ArrayList<Integer> listVisit = new ArrayList<>();
	protected ArrayList<Integer> vexVisited = new ArrayList<>();
	public Graph(int v) {
		this.numVex = v;
		this.adj = new int[numVex][numVex];
		visited = new boolean[numVex];
	}

	 // Tuan 1
    // add edg
    public abstract void addEdges(int src,int dest);
    //remove edg
    public abstract void delEdges(int src,int dest);
    // deg of vex
    public  abstract  int degreeVex(int anyVex);
    // deg of graph
    public abstract int degreeGraph();
    //number of edg
    public abstract int numEdgesGraph();
    //print
    public abstract void printGraph();
	// Tuan 2

	// Kiem tra do thi co phai la do thi luong phan
	public abstract boolean isBipartiteGraph();

	// Kiem tra do thi con
	public abstract boolean isSubGraph(Graph g1, Graph g2);

	// Duyet DFS duyet theo chieu sau de quy
	public abstract void re_DFS(int start);

	//Kiem tra lien thong
	public abstract boolean isConnection();

	// Dem thanh phan lien thong
	public abstract int countConnection();

	//Tim thanh phan lien thong
	public abstract void findConnection();

	// Lop canh
	class Edges {
		int src, des;

		@Override
		public String toString() {
			return "Edges [src=" + src + ", des=" + des + "]";
		}

		public int getSrc() {
			return src;
		}

		public void setSrc(int src) {
			this.src = src;
		}

		public int getDes() {
			return des;
		}

		public void setDes(int des) {
			this.des = des;
		}

		public Edges(int src, int des) {
			super();
			this.src = src;
			this.des = des;
		}

	}
}