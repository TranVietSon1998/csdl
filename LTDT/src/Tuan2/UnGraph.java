package Tuan2;

import java.util.ArrayList;

import java.util.Stack;

public class UnGraph extends Graph {
	String result;

	public UnGraph(int v) {
		super(v);
	}

	@Override
	// Them canh
	public void addEdges(int src, int dest) {
		if (src >= 0 && dest >= 0 && src < numVex && dest <= numVex) {
			adj[src - 1][dest - 1] = 1;
			adj[dest - 1][src - 1] = 1;
		}
	}

	@Override
	// Xoa canh
	public void delEdges(int src, int dest) {
		if (src >= 0 && dest >= 0 && src < numVex && dest <= numVex) {
			adj[src - 1][dest - 1] = 0;
			adj[dest - 1][src - 1] = 0;
		}
	}

	@Override
	// Bac 1 dinh bat ky
	public int degreeVex(int anyVex) {
		int sum = 0;
		if (anyVex > -1 && anyVex < adj.length) {
			for (int i = 0; i < adj[anyVex].length; i++) {
				sum += adj[i][anyVex];
			}
			return sum;
		} else {
			return -1;
		}

	}

	@Override
// bac cua do thi
	public int degreeGraph() {
		int sum = 0;
		for (int i = 0; i < adj.length; i++) {
			System.out.println("Bac cua dinh " + i + " la: " + degreeVex(i));
			sum += degreeVex(i);

		}
		System.out.println("Bac cua tat ca cac dinh " + sum);
		return sum;
	}

	@Override
	// So canh cua 1 do thi
	public int numEdgesGraph() {

		int sum = 0;
		for (int i = 0; i < adj.length; i++) {

			sum += degreeVex(i);

		}
		System.out.println("So canh cua do thi: " + sum / 2);
		return sum / 2;
	}

	@Override
	// In do thi bang ma tran ke
	public void printGraph() {
		System.out.println("Ma tran cua do thi\n");
		for (int i = 0; i < adj.length; i++) {
			for (int j = 0; j < adj[i].length; j++) {
				System.out.print(adj[i][j] + "    ");

			}
			System.out.println();
		}
	}

	@Override
	// Kiem tra do thi co luong phan hay khogn
	public boolean isBipartiteGraph() {
		int[] color = new int[numVex];
		// -1 no color,1 color one, 0 color 2
		for (int i = 0; i < color.length; i++) {
			color[i] = -1;
		}

		// step 2 color
		for (int i = 0; i < color.length; i++) {
			for (int j = 0; j < color.length; j++) {
				if (adj[i][j] != 0 && color[j] == -1) {
					if (color[i] == 1)
						color[j] = 0;
					else
						color[j] = 1;
				}
			}
		}

		// step 3 check neu co 2 dinh ke nhau cung mau thi ko luong phan
		for (int i = 0; i < color.length; i++) {
			for (int j = 0; j < color.length; j++) {
				if (adj[i][j] != 0 && color[i] == color[j]) {
					return false;
				}
			}
		}
		return true;
	}

	// Kiem tra do thi nay co chua trong do thi khac hay khong ?
	// So luong canh = nhau ?
	// Canh cua A giong canh cua B ?
	public boolean isContain(ArrayList<Edges> arr1, ArrayList<Edges> arr2) {
		int count = 0;
		for (int i = 0; i < arr1.size(); i++) {
			for (int j = 0; j < arr2.size(); j++) {
				if (arr1.get(i).toString().equals(arr2.get(j).toString())) {
					count++;
				}
			}
		}
		if (count == arr2.size()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	// Kiem tra 1 do thi co phai la con cua do thi khac
	public boolean isSubGraph(Graph g1, Graph g2) {
		int countG1 = g1.adj.length;
		int countG2 = g2.adj.length;
		ArrayList<Edges> listCha = new ArrayList<>();
		ArrayList<Edges> listCon = new ArrayList<>();

		for (int i = 0; i < numVex; i++) {
			for (int j = 0; j < numVex; j++) {
				if (g2.adj[i][j] > 0)
					listCha.add(new Edges(i + 1, j + 1));

			}
		}
		System.out.println(listCha);

		for (int k = 0; k < g1.adj.length; k++) {

			for (int m = 0; m < g1.adj.length; m++) {

				if (g1.adj[k][m] > 0)
					listCon.add(new Edges(k + 1, m + 1));

			}
		}
		System.out.println(listCon);
		if (countG1 <= countG2 && isContain(listCha, listCon))

			return true;
		else
			return false;

	}

// Duyet do thi theo chieu sau dung de quy
	@Override
	public void re_DFS(int startVex) {
		
		int V = startVex - 1;
		visited[V] = true;
		System.out.print((V+1)+"\t");
		for (int i = 0; i < numVex; i++) {
			if (adj[V][i] != 0 && visited[i] == false) {
				re_DFS(i + 1);
			}
		}

	}

	@Override
	// Kiem tra tinh lien thong cua do thi
	public boolean isConnection() {
		int startVex=1;
	
		int V = startVex - 1;
		visited[V] = true;
		
//		for (int i = 0; i < numVex; i++) {
//			if (adj[V][i] != 0 && visited[i] == false) {
//				re_DFS(i + 1);
//			}
//		}
		// check
		for (int i = 0; i < numVex; i++) {
			if (visited[i] == false)
				return false;
		}
		return true;
	}

	@Override
	// Dem so thanh phan lien thong
	public int countConnection() {
		int demLienThong = 1;
		int i = 0;
		int numberPoint = adj.length;
		Stack<Integer> stack = new Stack<Integer>();
		ArrayList<Integer> listVisit = new ArrayList<>();
		// dau tien la gan setTemp bang tat ca cac dinh co trong do thi
		ArrayList<Integer> setTmp = new ArrayList<>();
		for (int j = 0; j < adj.length; j++) {
			setTmp.add(j);
		}
		int[] visit = new int[numberPoint];
		visit[i] = 1;
		listVisit.add(i);
		stack.push(i);
		setTmp.remove(i);
		while (!stack.empty()) {
			int count = 0;
			i = stack.peek();
			for (int j = 0; j < visit.length; j++) {
				if (this.adj[i][j] > 0 && visit[j] != 1) {
					listVisit.add(j);
					visit[j] = 1;
					stack.push(j);
					// neu phan tu do da duyet roi thi xoa di
					for (int j2 = 0; j2 < setTmp.size(); j2++) {
						if (setTmp.get(j2) == j) {
							setTmp.remove(j2);
						}
					}
					break;
				} else {
					count++;
				}
			}
			if (count == visit.length) {
				// neu phan tu duyet xong roi thi break
				if (!setTmp.isEmpty()) {
					// chua thi lay phan tu dau tien nhat ra duyet tiep
					i = setTmp.get(0);
					if (i < adj.length) {
						demLienThong++;
						stack.removeAll(stack);
						// thuc hien push de stack khong rong
						stack.push(i);
						// remove lo trinh cu de ghi vao lo trinh moi
						listVisit.removeAll(listVisit);
					} else {
						break;
					}
				} else {
					break;
				}
			}
		}
		return demLienThong;


	}

	@Override
	// Tim cac thanh phan lien thong cua do thi
	public void findConnection() {
		int demLienThong = 1;
		int i = 0;
		int numberPoint = adj.length;
		Stack<Integer> stack = new Stack<Integer>();
		ArrayList<Integer> listVisit = new ArrayList<>();
		// dau tien la gan setTemp bang tat ca cac dinh co trong do thi
		ArrayList<Integer> setTmp = new ArrayList<>();
		for (int j = 0; j < adj.length; j++) {
			setTmp.add(j);
		}
		int[] visit = new int[numberPoint];
		visit[i] = 1;
		listVisit.add(i);
		stack.push(i);
		setTmp.remove(i);
		while (!stack.empty()) {
			int count = 0;
			i = stack.peek();
			for (int j = 0; j < visit.length; j++) {
				if (this.adj[i][j] > 0 && visit[j] != 1) {
					listVisit.add(j);
					visit[j] = 1;
					stack.push(j);
					// neu phan tu do da duyet roi thi xoa di
					for (int j2 = 0; j2 < setTmp.size(); j2++) {
						if (setTmp.get(j2) == j) {
							setTmp.remove(j2);
						}
					}
					break;
				} else {
					count++;
				}
			}
			if (count == visit.length) {
				// neu khong con duong di
				System.out.println("So thanh phan lien thong : " + demLienThong);
				// xuat ra lo trinh
				print(listVisit);
				// neu phan tu duyet xong roi thi break
				if (!setTmp.isEmpty()) {
					// chua thi lay phan tu dau tien nhat ra duyet tiep
					i = setTmp.get(0);
					if (i < adj.length) {
						demLienThong++;
						stack.removeAll(stack);
						// thuc hien push de stack khong rong
						stack.push(i);
						// remove lo trinh cu de ghi vao lo trinh moi
						listVisit.removeAll(listVisit);
					} else {
						break;
					}
				} else {
					break;
				}
			}
		}

	}

	private void print(ArrayList<Integer> listVisit) {
		String string = "";
		String temp = " ==> ";
		for (int k = 0; k < listVisit.size(); k++) {
			string += listVisit.get(k)+1;
			if (k < listVisit.size() - 1) {
				string += temp;
			}
		}
		System.out.println(string);

	}
}
