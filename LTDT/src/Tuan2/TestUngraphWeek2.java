package Tuan2;

public class TestUngraphWeek2 {
	public static void main(String[] args) {
		System.out.println("Exe week 2");
		System.out.println();
		UnGraph ug2 = new UnGraph(9);

		ug2.addEdges(1, 5);
		ug2.addEdges(1, 7);
		ug2.addEdges(2, 3);
		ug2.addEdges(2, 7);
		ug2.addEdges(2, 9);
		ug2.addEdges(3, 4);
		ug2.addEdges(4, 5);
		ug2.addEdges(4, 9);
		ug2.addEdges(5, 6);
		ug2.addEdges(5, 8);
		ug2.addEdges(6, 7);

		ug2.printGraph();
		System.out.println();

		System.out.println("Kiem Tra Do Thi Luong Phan???");
		System.out.println(ug2.isBipartiteGraph());

		System.out.println();
		UnGraph ug4 = new UnGraph(4);
		ug4.addEdges(1, 2);
		ug4.addEdges(1, 4);
		ug4.addEdges(2, 3);
		ug4.addEdges(3, 4);

		UnGraph ug5 = new UnGraph(4);
		ug5.addEdges(1, 2);
		ug5.addEdges(1, 4);

		System.out.println("Kiem Tra 1 Do Thi Co Phai La Con Cua Do Thi Khac Khong??? ");
		System.out.println(ug4.isSubGraph(ug5, ug4));

		System.out.println();

		UnGraph ug3 = new UnGraph(7);

		ug3.addEdges(1, 2);
		ug3.addEdges(1, 3);
		ug3.addEdges(1, 7);
		ug3.addEdges(2, 3);
		ug3.addEdges(2, 4);
		ug3.addEdges(2, 6);
		ug3.addEdges(3, 4);
		ug3.addEdges(3, 7);
		ug3.addEdges(4, 6);
		ug3.addEdges(4, 7);
		ug3.addEdges(5, 7);

		ug3.printGraph();

		System.out.println();

		System.out.println("Duy?t DFS: ");
		ug3.re_DFS(1);

		System.out.println();
		System.out.println("Do Thi Co Lien Thong Khong?");
		System.out.println(ug3.isConnection());
		System.out.println();
		System.out.println("So Thanh Phan Lien Thong");
		System.out.println(ug3.countConnection());
		System.out.println();
		System.out.println("Thanh Phan Lien Thong");
		ug3.findConnection();

	}
}
