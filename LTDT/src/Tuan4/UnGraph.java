package Tuan4;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class UnGraph extends Graph {
	String result;
	String s;

	public UnGraph(int v) {
		super(v);
	}

	// Bai tap tuan 1

	@Override
	// Them canh
	public void addEdges(int src, int dest) {
		if (src >= 0 && dest >= 0 && src <= numVex && dest <= numVex) {
			adj[src - 1][dest - 1] = 1;
			adj[dest - 1][src - 1] = 1;
		}
	}

	@Override
	// Xoa canh
	public void delEdges(int src, int dest) {
		if (src >= 0 && dest >= 0 && src <= numVex && dest <= numVex) {
			adj[src - 1][dest - 1] = 0;
			adj[dest - 1][src - 1] = 0;
		}
	}

	@Override
	// Bac 1 dinh bat ky
	public int degreeVex(int anyVex) {
		int sum = 0;
		if (anyVex > -1 && anyVex < adj.length) {
			for (int i = 0; i < adj[anyVex].length; i++) {
				sum += adj[i][anyVex];
			}
			return sum;
		} else {
			return -1;
		}

	}

	@Override
// bac cua do thi
	public int degreeGraph() {
		int sum = 0;
		for (int i = 0; i < adj.length; i++) {
			System.out.println("Bac cua dinh " + i + " la: " + degreeVex(i));
			sum += degreeVex(i);

		}
		System.out.println("Bac cua tat ca cac dinh " + sum);
		return sum;
	}

	@Override
	// So canh cua 1 do thi
	public int numEdgesGraph() {

		int sum = 0;
		for (int i = 0; i < adj.length; i++) {

			sum += degreeVex(i);

		}
		System.out.println("So canh cua do thi: " + sum / 2);
		return sum / 2;
	}

	@Override
	// In do thi bang ma tran ke
	public void printGraph() {
		System.out.println("Ma tran cua do thi\n");
		for (int i = 0; i < adj.length; i++) {
			for (int j = 0; j < adj[i].length; j++) {
				System.out.print(adj[i][j] + "    ");

			}
			System.out.println();
		}
	}

	// Bai tap tuan 2

	@Override
	// Kiem tra do thi co luong phan hay khogn
	public boolean isBipartiteGraph() {
		int[] color = new int[numVex];
		// -1 no color,1 color one, 0 color 2
		for (int i = 0; i < color.length; i++) {
			color[i] = -1;
		}

		// step 2 color
		for (int i = 0; i < color.length; i++) {
			for (int j = 0; j < color.length; j++) {
				if (adj[i][j] != 0 && color[j] == -1) {
					if (color[i] == 1)
						color[j] = 0;
					else
						color[j] = 1;
				}
			}
		}

		// step 3 check neu co 2 dinh ke nhau cung mau thi ko luong phan
		for (int i = 0; i < color.length; i++) {
			for (int j = 0; j < color.length; j++) {
				if (adj[i][j] != 0 && color[i] == color[j]) {
					return false;
				}
			}
		}
		return true;
	}

	// Kiem tra do thi nay co chua trong do thi khac hay khong ?
	// So luong canh = nhau ?
	// Canh cua A giong canh cua B ?
	public boolean isContain(ArrayList<Edges> arr1, ArrayList<Edges> arr2) {
		int count = 0;
		for (int i = 0; i < arr1.size(); i++) {
			for (int j = 0; j < arr2.size(); j++) {
				if (arr1.get(i).toString().equals(arr2.get(j).toString())) {
					count++;
				}
			}
		}
		if (count == arr2.size()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	// Kiem tra 1 do thi co phai la con cua do thi khac
	public boolean isSubGraph(Graph g1, Graph g2) {
		int countG1 = g1.adj.length;
		int countG2 = g2.adj.length;
		ArrayList<Edges> listCha = new ArrayList<>();
		ArrayList<Edges> listCon = new ArrayList<>();

		for (int i = 0; i < numVex; i++) {
			for (int j = 0; j < numVex; j++) {
				if (g2.adj[i][j] > 0)
					listCha.add(new Edges(i + 1, j + 1));

			}
		}
		System.out.println(listCha);

		for (int k = 0; k < g1.adj.length; k++) {

			for (int m = 0; m < g1.adj.length; m++) {

				if (g1.adj[k][m] > 0)
					listCon.add(new Edges(k + 1, m + 1));

			}
		}
		System.out.println(listCon);
		if (countG1 <= countG2 && isContain(listCha, listCon))

			return true;
		else
			return false;

	}

// Duyet do thi theo chieu sau dung de quy
	@Override
	public void re_DFS(int startVex) {

		int V = startVex - 1;
		visited[V] = true;
		System.out.print((V + 1) + "\t");
		for (int i = 0; i < numVex; i++) {
			if (adj[V][i] != 0 && visited[i] == false) {
				re_DFS(i + 1);
			}
		}

	}

	public void re_DFS2(int startVex) {

		int V = startVex - 1;
		visited[V] = true;

		for (int i = 0; i < numVex; i++) {
			if (adj[V][i] != 0 && visited[i] == false) {
				re_DFS2(i + 1);
			}
		}

	}

	@Override
	// Kiem tra tinh lien thong cua do thi
	public boolean isConnection() {

		re_DFS2(1);

		// check
		for (int i = 0; i < numVex; i++) {
			if (visited[i] == false)
				return false;
		}
		return true;
	}

	@Override
	// Dem so thanh phan lien thong
	public int countConnection() {
		int demLienThong = 1;
		int i = 0;
		int numberPoint = adj.length;
		Stack<Integer> stack = new Stack<Integer>();
		ArrayList<Integer> listVisit = new ArrayList<>();
		// dau tien la gan setTemp bang tat ca cac dinh co trong do thi
		ArrayList<Integer> setTmp = new ArrayList<>();
		for (int j = 0; j < adj.length; j++) {
			setTmp.add(j);
		}
		int[] visit = new int[numberPoint];
		visit[i] = 1;
		listVisit.add(i);
		stack.push(i);
		setTmp.remove(i);
		while (!stack.empty()) {
			int count = 0;
			i = stack.peek();
			for (int j = 0; j < visit.length; j++) {
				if (this.adj[i][j] > 0 && visit[j] != 1) {
					listVisit.add(j);
					visit[j] = 1;
					stack.push(j);
					// neu phan tu do da duyet roi thi xoa di
					for (int j2 = 0; j2 < setTmp.size(); j2++) {
						if (setTmp.get(j2) == j) {
							setTmp.remove(j2);
						}
					}
					break;
				} else {
					count++;
				}
			}
			if (count == visit.length) {
				// neu phan tu duyet xong roi thi break
				if (!setTmp.isEmpty()) {
					// chua thi lay phan tu dau tien nhat ra duyet tiep
					i = setTmp.get(0);
					if (i < adj.length) {
						demLienThong++;
						stack.removeAll(stack);
						// thuc hien push de stack khong rong
						stack.push(i);
						// remove lo trinh cu de ghi vao lo trinh moi
						listVisit.removeAll(listVisit);
					} else {
						break;
					}
				} else {
					break;
				}
			}
		}
		return demLienThong;

	}

	@Override
	// Tim cac thanh phan lien thong cua do thi
	public void findConnection() {
		int demLienThong = 1;
		int i = 0;
		int numberPoint = adj.length;
		Stack<Integer> stack = new Stack<Integer>();
		ArrayList<Integer> listVisit = new ArrayList<>();
		// dau tien la gan setTemp bang tat ca cac dinh co trong do thi
		ArrayList<Integer> setTmp = new ArrayList<>();
		for (int j = 0; j < adj.length; j++) {
			setTmp.add(j);
		}
		int[] visit = new int[numberPoint];
		visit[i] = 1;
		listVisit.add(i);
		stack.push(i);
		setTmp.remove(i);
		while (!stack.empty()) {
			int count = 0;
			i = stack.peek();
			for (int j = 0; j < visit.length; j++) {
				if (this.adj[i][j] > 0 && visit[j] != 1) {
					listVisit.add(j);
					visit[j] = 1;
					stack.push(j);
					// neu phan tu do da duyet roi thi xoa di
					for (int j2 = 0; j2 < setTmp.size(); j2++) {
						if (setTmp.get(j2) == j) {
							setTmp.remove(j2);
						}
					}
					break;
				} else {
					count++;
				}
			}
			if (count == visit.length) {
				// neu khong con duong di
				System.out.println("So thanh phan lien thong : " + demLienThong);
				// xuat ra lo trinh
				print(listVisit);
				// neu phan tu duyet xong roi thi break
				if (!setTmp.isEmpty()) {
					// chua thi lay phan tu dau tien nhat ra duyet tiep
					i = setTmp.get(0);
					if (i < adj.length) {
						demLienThong++;
						stack.removeAll(stack);
						// thuc hien push de stack khong rong
						stack.push(i);
						// remove lo trinh cu de ghi vao lo trinh moi
						listVisit.removeAll(listVisit);
					} else {
						break;
					}
				} else {
					break;
				}
			}
		}

	}

	// Bai tap tuan 3

	@Override
	public boolean isSubGraph2(Graph g1, Graph g2) {
		ArrayList<Edges> listSub = new ArrayList<>();
		ArrayList<Edges> listSup = new ArrayList<>();
		int count = 0;
		// Kiem tra dinh : dinh con <= dinh cha

		if (g1.numVex <= g2.numVex) {
			for (int i = 0; i < g1.numVex; i++) {
				for (int j = 0; j < g1.numVex; j++) {
					if (g1.adj[i][j] != 0) {
						listSub.add(new Edges(i, j));
					}
				}
			}
		}

		// Them canh la listSup
		for (int i = 0; i < g2.numVex; i++) {
			for (int j = 0; j < g2.numVex; j++) {
				if (g2.adj[i][j] != 0) {
					listSup.add(new Edges(i, j));
				}
			}
		}

// Do thi con co chua trong do thi cha hay ko
		for (int i = 0; i < listSub.size(); i++) {
			for (int j = 0; j < listSup.size(); j++) {
				if (listSub.get(i).toString().equalsIgnoreCase(listSup.get(j).toString())) {
					count++;
				}
			}
		}
		if (count == listSub.size()) {
			return true;
		}
		return false;
	}

	@Override
	// Stack LIFO
	public void DFS(int start) {
		System.out.println("Duyet DFS tu dinh: " + (start + 1));
		// stack de phong ngua truong hop quay lui ;
		Stack<Integer> stack = new Stack<>();
		// list de luu danh sach duong di da qua
		ArrayList<Integer> listVisit = new ArrayList<>();
		// arr danh dau dinh da qua
		int[] visit = new int[numVex];
		// dau tien add dinh da cho vao
		visit[start] = 1;
		listVisit.add(start);
		// Push bổ sung một phần tử vào đỉnh (top) của ngăn xếp,
		// nghĩa là sau các phần tử đã có trong ngăn xếp.
		// Pop giải phóng và trả về phần tử đang đứng ở đỉnh của ngăn xếp
		stack.push(start);
		// dừng khi stack rỗng
		while (!stack.empty()) {
			// bien count de kiem tra khi nao can lay phan tu ra khoi stack
			int count = 0;
			// peek kiểm tra xem đỉnh của danh sách có rỗng không
			// nếu rỗng thì trả về null còn không trả về giá trị của đỉnh danh sách.
			start = stack.peek();
			// duyet va kiem tra
			for (int j = 0; j < visit.length; j++) {
				if (this.adj[start][j] > 0 && visit[j] == 0) {
					// neu j chưa thăm thi them vao list
					listVisit.add(j);
					visit[j] = 1;
					stack.push(j);
					// neu them roi thi dung lai theo quy tac cuon
					break;
				} else {
					count++;
				}
			}
			if (count == visit.length) {
				stack.pop();
			}
		}
		print(listVisit);
	}

	private void print(ArrayList<Integer> listVisit) {
		String string = "";
		String temp = " ==> ";
		for (int k = 0; k < listVisit.size(); k++) {
			string += listVisit.get(k) + 1;
			if (k < listVisit.size() - 1) {
				string += temp;
			}
		}
		System.out.println(string);

	}

	@Override
	public void BFS(int start) {
		System.out.println("Duyet BFS tu dinh: " + (start + 1));
		Queue<Integer> queue = new LinkedList<>();
		boolean[] visited = new boolean[numVex];
		ArrayList<Integer> list = new ArrayList<>();
		visited[start] = true;
		list.add(start);
		queue.add(start);

		while (!queue.isEmpty()) {
			start = queue.remove();
			for (int i = 0; i < visited.length; i++) {
				if (this.adj[start][i] > 0 && visited[i] == false) {
					list.add(i);
					visited[i] = true;
					queue.add(i);

				}
			}

		}
		print(list);
	}

	@Override
	// Duyet theo chieu sau tim duong dai nhat
	public void findPathLong(int src, int dest) {
		System.out.println("Duong di dai nhat tu dinh: " + (src + 1) + " den: " + (dest + 1) + " la: ");

		// stack de phong ngua truong hop quay lui ;
		Stack<Integer> stack = new Stack<>();
		// list de luu danh sach duong di da qua
		ArrayList<Integer> listVisit = new ArrayList<>();
		// arr danh dau dinh da qua
		int[] visit = new int[numVex];
		// dau tien add dinh da cho vao
		visit[src] = 1;
		listVisit.add(src);
		// Push bổ sung một phần tử vào đỉnh (top) của ngăn xếp,
		// nghĩa là sau các phần tử đã có trong ngăn xếp.
		// Pop giải phóng và trả về phần tử đang đứng ở đỉnh của ngăn xếp
		stack.push(src);
		// dừng khi stack rỗng
		while (!stack.empty()) {
			// bien count de kiem tra khi nao can lay phan tu ra khoi stack
			int count = 0;
			// peek kiểm tra xem đỉnh của danh sách có rỗng không
			// nếu rỗng thì trả về null còn không trả về giá trị của đỉnh danh sách.
			src = stack.peek();

			if (src == dest) {
				break;
			}
			// duyet va kiem tra
			for (int j = 0; j < visit.length; j++) {
				if (this.adj[src][j] > 0 && visit[j] == 0) {
					// neu j chưa thăm thi them vao list
					listVisit.add(j);
					visit[j] = 1;
					stack.push(j);
					// neu them roi thi dung lai theo quy tac cuon
					break;
				} else {
					count++;
				}
			}
			if (count == visit.length) {
				stack.pop();
			}
		}
		print(listVisit);

	}

	@Override

	public void findPathShortest(int src, int dest) {
		System.out.println("Duong di ngan nhat tu dinh: " + (src + 1) + " den: " + (dest + 1) + " la: ");
		Queue<Integer> queue = new LinkedList<>();
		ArrayList<Integer> list = new ArrayList<>();
		visited[src] = true;
		list.add(src);
		queue.add(src);
		while (!queue.isEmpty()) {

			src = queue.poll(); // ở đây, lấy từ đằng sau
			for (int i = 0; i < numVex; i++) {
				if (this.adj[src][i] > 0 && visited[i] == false) {

					list.add(i);
					visited[i] = true;
					queue.add(i);
					if (i == dest) {
						queue.clear(); // xóa hết bắt dòng while dừng
						break;
					}

				}

			}
		}
		print(list);
	}

// Bai tap tuan 4	

	@Override
	public void findWay(int src, int dest) {
		System.out.println("Duong di dai nhat tu dinh: " + (src + 1) + " den: " + (dest + 1) + " la: ");

		// stack de phong ngua truong hop quay lui ;
		Stack<Integer> stack = new Stack<>();
		// list de luu danh sach duong di da qua
		ArrayList<Integer> listVisit = new ArrayList<>();
		// arr danh dau dinh da qua
		int[] visit = new int[numVex];
		// dau tien add dinh da cho vao
		visit[src] = 1;
		listVisit.add(src);
		// Push bổ sung một phần tử vào đỉnh (top) của ngăn xếp,
		// nghĩa là sau các phần tử đã có trong ngăn xếp.
		// Pop giải phóng và trả về phần tử đang đứng ở đỉnh của ngăn xếp
		stack.push(src);
		// dừng khi stack rỗng
		while (!stack.empty()) {
			// bien count de kiem tra khi nao can lay phan tu ra khoi stack
			int count = 0;
			// peek kiểm tra xem đỉnh của danh sách có rỗng không
			// nếu rỗng thì trả về null còn không trả về giá trị của đỉnh danh sách.
			src = stack.peek();

			if (src == dest) {
				break;
			}
			// duyet va kiem tra
			for (int j = 0; j < visit.length; j++) {
				if (this.adj[src][j] > 0 && visit[j] == 0) {
					// neu j chưa thăm thi them vao list
					listVisit.add(j);
					visit[j] = 1;
					stack.push(j);
					// neu them roi thi dung lai theo quy tac cuon
					break;
				} else {
					count++;
				}
			}
			if (count == visit.length) {
				stack.pop();
			}
		}
		print(listVisit);
	}

	@Override
	public void findWayShortest(int src, int dest) {
		System.out.println("Duong di ngan nhat tu dinh: " + (src + 1) + " den: " + (dest + 1) + " la: ");
		Queue<Integer> queue = new LinkedList<>();
		ArrayList<Integer> list = new ArrayList<>();
		visited[src] = true;
		list.add(src);
		queue.add(src);
		while (!queue.isEmpty()) {

			src = queue.poll(); // ở đây, lấy từ đằng sau
			for (int i = 0; i < numVex; i++) {
				if (this.adj[src][i] > 0 && visited[i] == false) {

					list.add(i);
					visited[i] = true;
					queue.add(i);
					if (i == dest) {
						queue.clear(); // xóa hết bắt dòng while dừng
						break;
					}

				}

			}
		}
		print(list);

	}

	@Override
	// Mot chu trinh euler co so dinh >1, lien thong manh, cac dinh deu la bac chan
	public boolean checkCycleEuler() {
		boolean result = false;
		int count = 0;

		for (int i = 0; i < numVex; i++) {
			if (degreeVex(i) % 2 == 0) {
				count++; // dem so dinh bac chan
			}

		}

		if (isConnection() == true && numVex > 1 && count == numVex) {
			result = true;
		}
		return result;
	}

	@Override
	public boolean checkPathEuler() {
		boolean result = false;
		int count = 0;

		for (int i = 0; i < numVex; i++) {
			if (degreeVex(i) % 2 != 0)
				count++;

		}
		if (isConnection() == true && numVex > 1 && count == 2) // Duong di euler co dung 2 dinh bac le,lien thong manh,
																// va co so dinh >1
			result = true;

		return result;
	}

	@Override
	public void findCycleEuler(int start) {
		if (checkCycleEuler() == false) {
			System.out.println("Khong co chu trinh euler");
		} else {
			System.out.println("Chu trinh Euler xuat phat tu dinh " + (start+1) + " la: ");
			Stack<Integer> stack = new Stack<Integer>();
			ArrayList<Integer> list = new ArrayList<Integer>(); // luu dinh da duyet

			stack.push(start);
			list.add(start);
			while (!stack.isEmpty()) {
				int x = stack.pop(); // lay ra

				for (int i = 0; i < numVex; i++) {
					if (adj[x][i] != 0) { 
						stack.push(i);
						list.add(i);
						adj[x][i] = adj[i][x] = 0;// xoa canh
						x = i;
						i = 0;//goi de chay lai tu dau
					}
					if (i == numVex && !stack.isEmpty()) { // chay het cac canh
						stack.pop();
					}
				}

			}
			print(list);
		}
	}

	@Override
	public void findPathEuler(int start) {
		ArrayList<Integer> listHasPathEuler = new ArrayList<>();
		if (checkPathEuler() == false) {
			System.out.println("Khong co duong di Euler");
		} else {
			if (degreeVex(start) % 2 == 0) {
				for (int i = 0; i < numVex; i++) {
					if (degreeVex(i) % 2 != 0) {
						listHasPathEuler.add(i);
					}
				}
				System.out.println("Khong co duong di tu dinh " + start);
				System.out.println("Danh sach dinh co duong di: " + listHasPathEuler.toString());
			} else {

				Stack<Integer> stack = new Stack<Integer>();
				ArrayList<Integer> list = new ArrayList<Integer>(); // luu dinh da duyet
				stack.push(start);
				list.add(start);
				
				while (!stack.isEmpty()) {
					int x = stack.pop(); // lay ra

					for (int j = 0; j < numVex; j++) {
						if (adj[x][j] != 0) { // Co the lap lai dinh nhung khong lap lai canh
							stack.push(j);
							list.add(j);
							adj[x][j] = adj[j][x] = 0;
							x = j;
							j = 0;
						}
						if (j == numVex && !stack.isEmpty()) { // chay het cac canh
							stack.pop();
						}
					}

				}
				print(list);
			}

		}
	}

}
