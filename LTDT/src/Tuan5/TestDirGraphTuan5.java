package Tuan5;

public class TestDirGraphTuan5 {
	public static void main(String[] args) {

		System.out.println("===============================TUAN5==============================");

		DirGraph dg = new DirGraph(4);
		dg.addEdges(1, 2);
		dg.addEdges(2, 3);
		dg.addEdges(3, 1);
		dg.addEdges(3, 4);
		dg.addEdges(4, 1);

		System.out.println("Kiem tra chu trinh Hamilton: " + dg.checkCycleHamilton());
		System.out.println("Kiem tra duong di Hamilton: " + dg.checkPathHamilton());
		dg.findCycleHamilton();

	}
}
