package Tuan5;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

public class DirGraph extends Graph {
	public DirGraph(int v) {
		super(v);
	}

	@Override
	public void addEdges(int src, int dest) {
		if (src >= 0 && dest >= 0 && src <= numVex && dest <= numVex) {
			adj[src - 1][dest - 1] = 1;
		}
	}

	@Override
	public void delEdges(int src, int dest) {
		if (src >= 0 && dest >= 0 && src <= numVex && dest <= numVex) {
			adj[src - 1][dest - 1] = 0;
		}
	}

	@Override
	public int degreeVex(int anyVex) {
		int sum = tongNuaBacNgoaiMotDinh(anyVex) + tongNuaBacTrongMotDinh(anyVex);
		return sum;
	}

	public int tongNuaBacNgoaiMotDinh(int anyVex) { // tong hang
		int sum = 0;
		if (anyVex >= 0 && anyVex < numVex) {
			for (int i = 0; i < numVex; i++) {
				sum += adj[anyVex][i];
			}
		}
		return sum;
	}

	public int tongNuaBacTrongMotDinh(int anyVex) { // tong cot
		int sum = 0;
		if (anyVex >= 0 && anyVex < numVex) {
			for (int i = 0; i < numVex; i++) {
				sum += adj[i][anyVex];
			}
		}
		return sum;
	}

	public int tongNuaBacTrongDoThi() {
		int sum = 0;
		for (int i = 0; i < adj.length; i++) {
			sum += tongNuaBacTrongMotDinh(i);
		}
		return sum;
	}

	public int tongNuaBacNgoaiDoThi() {
		int sum = 0;
		for (int i = 0; i < adj.length; i++) {
			sum += tongNuaBacNgoaiMotDinh(i);
		}
		return sum;
	}

	@Override
	public int degreeGraph() {
		int sum = 0;
		for (int i = 0; i < adj.length; i++) {
			System.out.println("Bac cua dinh " + i + " la: " + degreeVex(i));
			sum += degreeVex(i);
		}
		System.out.println("Bac cua tat ca cac dinh " + sum);
		return sum;

	}

	@Override
// So cung cua do thi co huong = tong bac ngoai = tong bac trong
	public int numEdgesGraph() {
		int sum = 0;
		for (int i = 0; i < adj.length; i++) {
			sum += tongNuaBacNgoaiMotDinh(i);
		}
		System.out.println("So cung cua do thi la: " + sum);
		return sum;

	}

	@Override
	public void printGraph() {
		System.out.println("Ma tran cua do thi\n");
		for (int i = 0; i < adj.length; i++) {
			for (int j = 0; j < adj[i].length; j++) {
				System.out.print(adj[i][j] + "    ");
			}
			System.out.println();
		}
	}

	// Bai tap tuan 2

	@Override
	public boolean isBipartiteGraph() {
		chuyenDoiMaTran(this);
		int[] color = new int[numVex];
		// -1 no color,1 color one, 0 color 2
		for (int i = 0; i < color.length; i++) {
			color[i] = -1;
		}
		// step 2 color
		for (int i = 0; i < color.length; i++) {
			for (int j = 0; j < color.length; j++) {
				if (adj[i][j] != 0 && color[j] == -1) {
					if (color[i] == 1)
						color[j] = 0;
					else
						color[j] = 1;
				}
			}
		}
		// step 3 check neu co 2 dinh ke nhau cung mau thi ko luong phan
		for (int i = 0; i < color.length; i++) {
			for (int j = 0; j < color.length; j++) {
				if (adj[i][j] != 0 && color[i] == color[j]) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public boolean isSubGraph(Graph g1, Graph g2) {
		ArrayList<Edges> listSub = new ArrayList<>();
		ArrayList<Edges> listSup = new ArrayList<>();
		int count = 0;
		// Kiem tra dinh : dinh con <= dinh cha
		if (g1.numVex <= g2.numVex) {
			for (int i = 0; i < g1.numVex; i++) {
				for (int j = 0; j < g1.numVex; j++) {
					if (g1.adj[i][j] != 0) {
						listSub.add(new Edges(i, j));
					}
				}
			}
		}

		// Them canh la listSup
		for (int i = 0; i < g2.numVex; i++) {
			for (int j = 0; j < g2.numVex; j++) {
				if (g2.adj[i][j] != 0) {
					listSup.add(new Edges(i, j));
				}
			}
		}
		// Do thi con co chua trong do thi cha hay ko
		for (int i = 0; i < listSub.size(); i++) {
			for (int j = 0; j < listSup.size(); j++) {
				if (listSub.get(i).toString().equalsIgnoreCase(listSup.get(j).toString())) {
					count++;
				}
			}
		}
		if (count == listSub.size()) {
			return true;
		}
		return false;
	}

	@Override
	public void re_DFS(int startVex) {
		int V = startVex - 1;
		visited[V] = true;
		// System.out.print((V + 1) + "\t");
		for (int i = 0; i < numVex; i++) {
			if (adj[V][i] != 0 && visited[i] == false) {
				re_DFS(i + 1);
			}
		}

	}

// Chuyen doi ma tran co huong ve ma tran vo huong
	public void chuyenDoiMaTran(DirGraph g) {
		int[][] test = g.adj;
		for (int i = 0; i < test.length; i++) {
			for (int j = 0; j < test.length; j++) {
				if (test[i][j] != 0) {
					this.adj[i][j] = test[i][j];
					this.adj[j][i] = test[j][i];
				}
			}
		}
	}

	// Kiem tra do thi lien thong yeu
	public boolean ktDoThiLienThongYeu() {
		chuyenDoiMaTran(this);
		re_DFS(1);
		// check
		for (int i = 0; i < numVex; i++) {
			if (visited[i] == false)
				return false;
		}
		return true;

	}

	// Kiem tra do thi lien thong manh
	public boolean ktDoThiLienThongManh() {
		boolean right = true;
		for (int k = 0; k < visited.length; k++) {
			if (tongNuaBacNgoaiMotDinh(k) == 0 || tongNuaBacTrongMotDinh(k) == 0) {
				right = false;
			}
//			if (visited[k] != true) {
//				right = false;
//			}
		}
		if (isConnection() && right) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean isConnection() {
		int i = 0;

		Stack<Integer> stack = new Stack<Integer>();
		ArrayList<Integer> listVisit = new ArrayList<>();

		visited[i] = true;
		listVisit.add(i);
		stack.push(i);
		while (!stack.empty()) {
			int count = 0;
			i = stack.peek();
			for (int j = 0; j < visited.length; j++) {
				if (this.adj[i][j] > 0 && visited[j] == false) {
					listVisit.add(j);
					visited[j] = true;
					stack.push(j);
					break;
				} else {
					count++;
				}
			}
			if (count == visited.length) {
				stack.pop();
			}
		}
		for (int j = 0; j < visited.length; j++) {
			if (visited[j] != true)
				return false;
		}
		return true;
	}

	@Override
	public int countConnection() {
		int demLienThong = 1;
		int i = 0;
		int numberPoint = adj.length;
		Stack<Integer> stack = new Stack<Integer>();
		ArrayList<Integer> listVisit = new ArrayList<>();
		// dau tien la gan setTemp bang tat ca cac dinh co trong do thi
		ArrayList<Integer> setTmp = new ArrayList<>();
		for (int j = 0; j < adj.length; j++) {
			setTmp.add(j);
		}
		int[] visit = new int[numberPoint];
		visit[i] = 1;
		listVisit.add(i);
		stack.push(i);
		setTmp.remove(i);
		while (!stack.empty()) {
			int count = 0;
			i = stack.peek();
			for (int j = 0; j < visit.length; j++) {
				if (this.adj[i][j] > 0 && visit[j] != 1) {
					listVisit.add(j);
					visit[j] = 1;
					stack.push(j);
					// neu phan tu do da duyet roi thi xoa di
					for (int j2 = 0; j2 < setTmp.size(); j2++) {
						if (setTmp.get(j2) == j) {
							setTmp.remove(j2);
						}
					}
					break;
				} else {
					count++;
				}
			}
			if (count == visit.length) {
				// neu phan tu duyet xong roi thi break
				if (!setTmp.isEmpty()) {
					// chua thi lay phan tu dau tien nhat ra duyet tiep
					i = setTmp.get(0);
					if (i < adj.length) {
						demLienThong++;
						stack.removeAll(stack);
						// thuc hien push de stack khong rong
						stack.push(i);
						// remove lo trinh cu de ghi vao lo trinh moi
						listVisit.removeAll(listVisit);
					} else {
						break;
					}
				} else {
					break;
				}
			}
		}
		return demLienThong;
	}

	@Override
	public void findConnection() {
		int demLienThong = 1;
		int i = 0;
		int numberPoint = adj.length;
		Stack<Integer> stack = new Stack<Integer>();
		ArrayList<Integer> listVisit = new ArrayList<>();
		// dau tien la gan setTemp bang tat ca cac dinh co trong do thi
		ArrayList<Integer> setTmp = new ArrayList<>();
		for (int j = 0; j < adj.length; j++) {
			setTmp.add(j);
		}
		int[] visit = new int[numberPoint];
		visit[i] = 1;
		listVisit.add(i);
		stack.push(i);
		setTmp.remove(i);
		while (!stack.empty()) {
			int count = 0;
			i = stack.peek();
			for (int j = 0; j < visit.length; j++) {
				if (this.adj[i][j] > 0 && visit[j] != 1) {
					listVisit.add(j);
					visit[j] = 1;
					stack.push(j);
					// neu phan tu do da duyet roi thi xoa di
					for (int j2 = 0; j2 < setTmp.size(); j2++) {
						if (setTmp.get(j2) == j) {
							setTmp.remove(j2);
						}
					}
					break;
				} else {
					count++;
				}
			}
			if (count == visit.length) {
				// neu khong con duong di
				System.out.println("So thanh phan lien thong : " + demLienThong);
				// xuat ra lo trinh
				print(listVisit);
				// neu phan tu duyet xong roi thi break
				if (!setTmp.isEmpty()) {
					// chua thi lay phan tu dau tien nhat ra duyet tiep
					i = setTmp.get(0);
					if (i < adj.length) {
						demLienThong++;
						stack.removeAll(stack);
						// thuc hien push de stack khong rong
						stack.push(i);
						// remove lo trinh cu de ghi vao lo trinh moi
						listVisit.removeAll(listVisit);
					} else {
						break;
					}
				} else {
					break;
				}
			}
		}

	}

	private void print(ArrayList<Integer> listVisit) {
		String string = "";
		String temp = " ==> ";
		for (int k = 0; k < listVisit.size(); k++) {
			string += listVisit.get(k) + 1;
			if (k < listVisit.size() - 1) {
				string += temp;
			}
		}
		System.out.println(string);
	}

	// Bai tap tuan 3

	@Override
	public boolean isSubGraph2(Graph g1, Graph g2) {
		ArrayList<Edges> listSub = new ArrayList<>();
		ArrayList<Edges> listSup = new ArrayList<>();
		int count = 0;
		// Kiem tra dinh : dinh con <= dinh cha
		if (g1.numVex <= g2.numVex) {
			for (int i = 0; i < g1.numVex; i++) {
				for (int j = 0; j < g1.numVex; j++) {
					if (g1.adj[i][j] != 0) {
						listSub.add(new Edges(i, j));
					}
				}
			}
		}

		// Them canh la listSup
		for (int i = 0; i < g2.numVex; i++) {
			for (int j = 0; j < g2.numVex; j++) {
				if (g2.adj[i][j] != 0) {
					listSup.add(new Edges(i, j));
				}
			}
		}
		// Do thi con co chua trong do thi cha hay ko
		for (int i = 0; i < listSub.size(); i++) {
			for (int j = 0; j < listSup.size(); j++) {
				if (listSub.get(i).toString().equalsIgnoreCase(listSup.get(j).toString())) {
					count++;
				}
			}
		}
		if (count == listSub.size()) {
			return true;
		}
		return false;
	}

	@Override
	public void DFS(int start) {
		System.out.println("Duyet DFS tu dinh: " + (start + 1));
		// stack de phong ngua truong hop quay lui ;
		Stack<Integer> stack = new Stack<>();
		// list de luu danh sach duong di da qua
		ArrayList<Integer> listVisit = new ArrayList<>();
		// arr danh dau dinh da qua
		int[] visit = new int[numVex];
		// dau tien add dinh da cho vao
		visit[start] = 1;
		listVisit.add(start);
		// Push bổ sung một phần tử vào đỉnh (top) của ngăn xếp,
		// nghĩa là sau các phần tử đã có trong ngăn xếp.
		// Pop giải phóng và trả về phần tử đang đứng ở đỉnh của ngăn xếp
		stack.push(start);
		// dừng khi stack rỗng
		while (!stack.empty()) {
			// bien count de kiem tra khi nao can lay phan tu ra khoi stack
			int count = 0;
			// peek kiểm tra xem đỉnh của danh sách có rỗng không
			// nếu rỗng thì trả về null còn không trả về giá trị của đỉnh danh sách.
			start = stack.peek();
			// duyet va kiem tra
			for (int j = 0; j < visit.length; j++) {
				if (this.adj[start][j] != 0 && visit[j] == 0) {
					// neu j chưa thăm thi them vao list
					listVisit.add(j);
					visit[j] = 1;
					stack.push(j);
					// neu them roi thi dung lai theo quy tac cuon
					break;
				} else {
					count++;
				}
			}
			if (count == visit.length) {
				stack.pop();
			}
		}
		print(listVisit);

	}

	@Override
	public void BFS(int start) {
		System.out.println("Duyet BFS tu dinh: " + (start + 1));
		Queue<Integer> queue = new LinkedList<>();
		boolean[] visited = new boolean[numVex];
		ArrayList<Integer> list = new ArrayList<>();
		visited[start] = true;
		list.add(start);
		queue.add(start);

		while (!queue.isEmpty()) {
			start = queue.remove();
			for (int i = 0; i < visited.length; i++) {
				if (this.adj[start][i] != 0 && visited[i] == false) {
					list.add(i);
					visited[i] = true;
					queue.add(i);
				}
			}
		}
		print(list);
	}

	@Override
	public void findPathLong(int src, int dest) {
		System.out.println("Duong di dai nhat tu dinh: " + (src + 1) + " den: " + (dest + 1) + " la: ");

		// stack de phong ngua truong hop quay lui ;
		Stack<Integer> stack = new Stack<>();
		// list de luu danh sach duong di da qua
		ArrayList<Integer> listVisit = new ArrayList<>();
		// arr danh dau dinh da qua
		int[] visit = new int[numVex];
		// dau tien add dinh da cho vao
		visit[src] = 1;
		listVisit.add(src);
		// Push bổ sung một phần tử vào đỉnh (top) của ngăn xếp,
		// nghĩa là sau các phần tử đã có trong ngăn xếp.
		// Pop giải phóng và trả về phần tử đang đứng ở đỉnh của ngăn xếp
		stack.push(src);
		// dừng khi stack rỗng
		while (!stack.empty()) {
			// bien count de kiem tra khi nao can lay phan tu ra khoi stack
			int count = 0;
			// peek kiểm tra xem đỉnh của danh sách có rỗng không
			// nếu rỗng thì trả về null còn không trả về giá trị của đỉnh danh sách.
			src = stack.peek();
			if (src == dest) {
				break;
			}
			// duyet va kiem tra
			for (int j = 0; j < visit.length; j++) {
				if (this.adj[src][j] != 0 && visit[j] == 0) {
					// neu j chưa thăm thi them vao list
					listVisit.add(j);
					visit[j] = 1;
					stack.push(j);
					// neu them roi thi dung lai theo quy tac cuon
					break;
				} else {
					count++;
				}
			}
			if (count == visit.length) {
				stack.pop();
			}
		}
		print(listVisit);
	}

	@Override
	public void findPathShortest(int src, int dest) {
		System.out.println("Duong di ngan nhat tu dinh: " + (src + 1) + " den: " + (dest + 1) + " la: ");
		Queue<Integer> queue = new LinkedList<>();
		ArrayList<Integer> list = new ArrayList<>();
		visited[src] = true;
		list.add(src);
		queue.add(src);
		while (!queue.isEmpty()) {
			src = queue.poll(); // ở đây, lấy từ đằng sau
			for (int i = 0; i < numVex; i++) {
				if (this.adj[src][i] != 0 && visited[i] == false) {

					list.add(i);
					visited[i] = true;
					queue.add(i);
					if (i == dest) {
						queue.clear(); // xóa hết bắt dòng while dừng
						break;
					}
				}
			}
		}
		print(list);
	}

	// Bai tap tuan 4

	@Override
	public void findWay(int src, int dest) {
		System.out.println("Duong di dai nhat tu dinh: " + (src + 1) + " den: " + (dest + 1) + " la: ");
		// stack de phong ngua truong hop quay lui ;
		Stack<Integer> stack = new Stack<>();
		// list de luu danh sach duong di da qua
		ArrayList<Integer> listVisit = new ArrayList<>();
		// arr danh dau dinh da qua
		int[] visit = new int[numVex];
		// dau tien add dinh da cho vao
		visit[src] = 1;
		listVisit.add(src);
		// Push bổ sung một phần tử vào đỉnh (top) của ngăn xếp,
		// nghĩa là sau các phần tử đã có trong ngăn xếp.
		// Pop giải phóng và trả về phần tử đang đứng ở đỉnh của ngăn xếp
		stack.push(src);
		// dừng khi stack rỗng
		while (!stack.empty()) {
			// bien count de kiem tra khi nao can lay phan tu ra khoi stack
			int count = 0;
			// peek kiểm tra xem đỉnh của danh sách có rỗng không
			// nếu rỗng thì trả về null còn không trả về giá trị của đỉnh danh sách.
			src = stack.peek();

			if (src == dest) {
				break;
			}
			// duyet va kiem tra
			for (int j = 0; j < visit.length; j++) {
				if (this.adj[src][j] != 0 && visit[j] == 0) {
					// neu j chưa thăm thi them vao list
					listVisit.add(j);
					visit[j] = 1;
					stack.push(j);
					// neu them roi thi dung lai theo quy tac cuon
					break;
				} else {
					count++;
				}
			}
			if (count == visit.length) {
				stack.pop();
			}
		}
		print(listVisit);

	}

	@Override
	public void findWayShortest(int src, int dest) {
		System.out.println("Duong di ngan nhat tu dinh: " + (src + 1) + " den: " + (dest + 1) + " la: ");
		Queue<Integer> queue = new LinkedList<>();
		ArrayList<Integer> list = new ArrayList<>();
		visited[src] = true;
		list.add(src);
		queue.add(src);
		while (!queue.isEmpty()) {
			src = queue.poll(); // ở đây, lấy từ đằng sau
			for (int i = 0; i < numVex; i++) {
				if (this.adj[src][i] != 0 && visited[i] == false) {

					list.add(i);
					visited[i] = true;
					queue.add(i);
					if (i == dest) {
						queue.clear(); // xóa hết bắt dòng while dừng
						break;
					}
				}
			}
		}
		print(list);
	}

	@Override
	public boolean checkCycleEuler() {
		int count = 0;
		for (int i = 0; i < numVex; i++) {
			if (tongNuaBacNgoaiMotDinh(i) == tongNuaBacTrongMotDinh(i))
				count++;
		}
		if (ktDoThiLienThongYeu() == true && numVex > 1 && count == numVex)
			return true;
		return false;
	}

	@Override
	public boolean checkPathEuler() {
		int count = 0;
		int canBang = 0;
		for (int i = 0; i < numVex; i++) {
			// 2 dinh thoa dieu kien
			if (tongNuaBacNgoaiMotDinh(i) == (tongNuaBacTrongMotDinh(i) + 1)
					|| tongNuaBacTrongMotDinh(i) == (tongNuaBacNgoaiMotDinh(i) + 1)) {
				count++;
			}
			// cac dinh con lai can bang
			if (tongNuaBacTrongMotDinh(i) == tongNuaBacNgoaiMotDinh(i)) {
				canBang++;
			}
		}
		if (ktDoThiLienThongYeu() == true & count == 2 && canBang == numVex - 2) {
			return true;
		}
		return false;
	}

	@Override
	public void findCycleEuler(int start) {
		if (checkCycleEuler() == false) {
			System.out.println("Khong co chu trinh euler");
		} else {
			System.out.println("Chu trinh Euler la: ");
			Stack<Integer> stack = new Stack<Integer>();
			ArrayList<Integer> list = new ArrayList<Integer>(); // luu dinh da duyet
			stack.push(start);
			list.add(start);
			while (!stack.isEmpty()) {
				int x = stack.pop(); // lay ra

				for (int i = 0; i < numVex; i++) {
					if (adj[x][i] != 0) { // Co the lap lai dinh nhung khong lap lai canh
						stack.push(i);
						list.add(i);
						adj[x][i] = 0;
						x = i;
						i = 0;

					}
					if (i == numVex && !stack.isEmpty()) { // chay het cac canh
						stack.pop();
					}
				}

			}
			print(list);
		}
	}

	@Override
	public void findPathEuler(int start) {
		ArrayList<Integer> listHasPathEuler = new ArrayList<>();
		if (checkPathEuler() == false) {
			System.out.println("Khong co duong di Euler");
		} else {
			if (degreeVex(start) % 2 == 0) {
				for (int i = 0; i < numVex; i++) {
					if (degreeVex(i) % 2 != 0) {
						listHasPathEuler.add(i);
					}
				}
				System.out.println("Khong co duong di tu dinh " + start);
				System.out.println("Danh sach dinh co duong di: " + listHasPathEuler.toString());
			} else {
				Stack<Integer> stack = new Stack<Integer>();
				ArrayList<Integer> list = new ArrayList<Integer>(); // luu dinh da duyet
				stack.push(start);
				list.add(start);
				while (!stack.isEmpty()) {
					int x = stack.pop(); // lay ra
					for (int j = 0; j < numVex; j++) {
						if (adj[x][j] != 0) { // Co the lap lai dinh nhung khong lap lai canh
							stack.push(j);
							list.add(j);
							adj[x][j] = 0;
							x = j;
							j = 0;
							break;
						}
						if (j == numVex && !stack.isEmpty()) { // chay het cac canh
							stack.pop();
						}
					}
				}
				print(list);
			}

		}

	}

	// Bai tap tuan 5

	// kiem tra chu trinh hamilton
	@Override
	/*
	 * Mọi đồ thị có hướng, có n đỉnh, liên thông mạnh. Nếu mỗi đỉnh v thuộc đồ thị
	 * thỏa: deg-(v)≥n/2 và deg+(v)≥n/2
	 */
	public boolean checkCycleHamilton() {
		int count = 0;
		if (ktDoThiLienThongManh()) {
			return true;
		}
		for (int i = 0; i < numVex; i++) {

			if (tongNuaBacNgoaiMotDinh(i) >= (numVex / 2) && tongNuaBacTrongMotDinh(i) >= (numVex / 2)) {
//				System.out.println(tongNuaBacNgoaiMotDinh(i));
//				System.out.println(tongNuaBacTrongMotDinh(i));
				count++;
			}
		}
		if (count == numVex) {
			return true;
		}

		return false;
	}

	List<Integer> listVisitHamiltonian = new ArrayList<>();

	public boolean dfs_haminton(int v, int instack_count) {

		if (instack_count == numVex)
			return true;
		for (int i = 0; i < numVex; i++)
			if (this.adj[v][i] > 0 && visited[i] == false) {
				visited[i] = true;
				if (dfs_haminton(i, instack_count + 1)) {
					listVisitHamiltonian.add(i);
					return true;
				}
				visited[i] = false;
			}
		return false;
	}

	// kiem tra duong di hamilton
	@Override
	public boolean checkPathHamilton() { // Y tuong NLP
		int count = 0;
		for (int i = 0; i < numVex; i++)
			visited[i] = false;
		for (int i = 0; i < numVex; i++) {
			visited[i] = true;
			if (dfs_haminton(i, 1)) {
				listVisitHamiltonian.add(i);
				for (int k = listVisitHamiltonian.size() - 1; k >= 0; k--)
					//System.out.print(listVisitHamiltonian.get(k) + " \t ");
			//	System.out.println();
				listVisitHamiltonian.removeAll(listVisitHamiltonian);
				for (int j = 0; j < numVex; j++)
					visited[j] = false;
				count++;
			}
			visited[i] = false;
		}
		return (count > 0) ? true : false;
	}

	// In ra 1 chu trinh hamilton

	int[] hc = new int[numVex];

	public void findCycleHamilton() {

		for (int i = 0; i < adj.length; i++)
			visited[i] = false;
		hc[0] = 0;
		visited[0] = true;
		Expand(1);

	}

	private void Expand(int i) {
		for (int j = 0; j < numVex; j++)
			if (!visited[j] && this.adj[hc[i - 1]][j] > 0) {
				hc[i] = j;
				if (i < numVex - 1) {
					visited[j] = true;
					Expand(i + 1);
					visited[j] = false;
				} else if (this.adj[hc[i]][0] > 0) {
					printHamiltonCycle(hc);
				}
			}

	}

	private void printHamiltonCycle(int[] hc2) {
		System.out.println("Chu trinh Hamilton: ");
		String string = "";
		String temp = " ==> ";
		for (int k = 0; k < hc2.length; k++) {
			string += hc2[k] + 1;
			if (k < hc2.length - 1) {
				string += temp;
			}
		}
		System.out.println(string + temp + (hc2[0] + 1));

	}

}
