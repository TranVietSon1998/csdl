package report_ltdt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

public class UnGraph extends Graph {
	String result;

	public UnGraph(int v) {
		super(v);
	}

	// =========================Bai Tap Tuan1============================

	@Override
	// Them canh
	public void addEdges(int src, int dest) {
		if (src >= 0 && dest >= 0 && src <= numVex && dest <= numVex) {
			adj[src - 1][dest - 1] = 1;
			adj[dest - 1][src - 1] = 1;
		}
	}

	// Them canh co trong so

	public void addEdges(int src, int dest, int weight) {
		if (src >= 0 && dest >= 0 && src <= numVex && dest <= numVex) {
			adj[src - 1][dest - 1] = weight;
			adj[dest - 1][src - 1] = weight;
		}
	}


	
	

	public void Dijsktra(int[][]matrix,int anyVex) {
		int[] P = new int[numVex];// lưu trữ các đỉnh
		int[] L = new int[numVex];// lưu trữ độ dài ngắn nhất giữa 2 đỉnh bất kỳ
		boolean[] R = new boolean[numVex];// đánh dấu đỉnh đã đi
		for (int i = 0; i < R.length; i++) {
			L[i] = Integer.MAX_VALUE;//thay khoảng cách các đỉnh về vô cùng
			R[i] = false;
		}
		L[anyVex] = 0;//gán khoảng cách 0 cho đỉnh đầu tiên
		P[anyVex] = -1;//đỉnh trước là null
		for (int i = 0; i < numVex; i++) {

			// tim dinh tiep theo de duyet
			int u = minDisTance(L, R);
			
			R[u] = true;//lấy ra được rồi thì đánh dấu đã thăm
			for (int j = 0; j < numVex; j++) {//lất các đỉnh tiếp theo
				if (R[j] == false && adj[u][j] != 0 && L[u] != Integer.MAX_VALUE && L[u] + adj[u][j] < L[j]) {//nếu đỉnh tiếp theo chưa thăm và 
					//trọng số khác ko và
					//khoảng cách khác vô cùng và trọng sô
					P[j] = u;//cập nhật đỉnh có giá trị nhỏ nhất cho p
					L[j] = L[u] + adj[u][j];//cập nhật lại khoảng cách
				}
			}
		}
		printPart(L, P, 0);
	}

	public void printPart(int[] L, int[] P, int startVex) {
		for (int i = 0; i < P.length; i++) {//lấy ra giá trị mảng P.chứa các đỉnh liền trước đỉnh đang xét
			System.out.print("\n duong di ngan nhat tu ");
			int goVex = i;
			Stack<Integer> stack = new Stack<Integer>();
			stack.push(goVex);
			while (startVex != goVex) {//so sánh
				stack.push(P[goVex]);//push đỉnh zô stack
				goVex = P[goVex];//gán đỉnh có giá trị nhỏ nhất
			}
			System.out.print(stack.pop());
		
			while (!stack.isEmpty()) {
				System.out.print("--->" + stack.pop());
			}
			System.out.print("\n Do dai = " + L[i]);
		}
	}

	public int minDisTance(int L[], boolean R[]) {
		int min = Integer.MAX_VALUE;//gán vô cùng
		int min_index = -1;//đỉnh nhỏ nhất
		for (int i = 0; i < numVex; i++) {
			if (R[i] == false && L[i] <= min) {
				min = L[i];//cập nhật giá min
				min_index = i;//gán đỉnh có khoảng cách nhỏ nhất
			}
		}
		return min_index;
	}


	@Override
	public int[][] Floyd(int[][] a) {
		int dist[][] = new int[numVex][numVex];
		for (int i = 0; i < dist.length; i++) {
			for (int j = 0; j < dist.length; j++) {
				if (a[i][j] == 0) {
					dist[i][j] = 1000;
				} else {
					dist[i][j] = a[i][j];
				}
			}
		}

		for (int k = 0; k < dist.length; k++) {

			for (int i = 0; i < dist.length; i++) {
				for (int j = 0; j < dist.length; j++) {
					if (dist[i][k] + dist[k][j] < dist[i][j]) {
						dist[i][j] = dist[i][k] + dist[k][j];
					}
				}
			}
		}
		printMatrixFloyd(dist);
		return dist;
	}
	public void printMatrixFloyd(int dist[][]) {
		System.out.println("ma tran Floyd");
		for (int i = 0; i < numVex; ++i) {
			for (int j = 0; j < numVex; ++j) {
				if (dist[i][j] >= 1000) {
					System.out.print("∞   ");
				} else {
					System.out.print(dist[i][j] + "   ");
				}
			}
			System.out.println();
		}
	}

	@Override
	public void delEdges(int src, int dest) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int degreeVex(int anyVex) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int degreeGraph() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int numEdgesGraph() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void printGraph() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isBipartiteGraph() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isSubGraph(Graph g1, Graph g2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void re_DFS(int start) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isConnection() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int countConnection() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void findConnection() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isSubGraph2(Graph g1, Graph g2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void DFS(int start) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void BFS(int start) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void findPathLong(int a, int b) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void findPathShortest(int a, int b) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void findWay(int a, int b) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void findWayShortest(int a, int b) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean checkCycleEuler() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean checkPathEuler() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void findCycleEuler(int start) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void findPathEuler(int start) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean checkCycleHamilton() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean checkPathHamilton() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void findCycleHamilton() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void findAllCycleHamilton() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void findAllCycleHamilton(int v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void findAllPathHamilton() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void findAllPathHamilton(int v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Graph DFSTree(int v) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Graph BFSTree(int v) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean hasCycle(int a, int b) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Graph Kruskal() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Graph Prim(int v) {
		// TODO Auto-generated method stub
		return null;
	}
}
