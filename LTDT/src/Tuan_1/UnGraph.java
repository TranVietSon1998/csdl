package Tuan_1;

public class UnGraph extends Graph {

	public UnGraph(int v) {
		super(v);
	}

	@Override// tham số:điểm đầu,điểm cuối
	public void addEdges(int src, int dest) {
		if (src >= 0 && dest >= 0 && src < numVex && dest <= numVex) {
			adj[src -1][dest - 1] =adj[dest - 1][src - 1]= 1;
           
		}
	}

	@Override
	public void delEdges(int src, int dest) {
		if (src >= 0 && dest >= 0 && src < numVex && dest <= numVex) {
			adj[src - 1][dest - 1] = 0;
			adj[dest - 1][src - 1] = 0;
		}
	}

	@Override
	public int degreeVex(int anyVex) {
		int sum = 0;
		if (anyVex >= 0 && anyVex < adj.length) {
			for (int i = 0; i < adj[anyVex].length; i++) {
				sum += adj[i][anyVex];
			}
			return sum;
		} else {
			return -1;
		}

	}

	@Override
	
	public int degreeGraph() {
		int sum = 0;
		for (int i = 0; i < adj.length; i++) {
			System.out.println("Deg of Vex " + i + " la: " + degreeVex(i));
			sum += degreeVex(i);

		}
                                //deg of totalVex
		System.out.println("Bac cua tat ca cac dinh " + sum);
		return sum;
	}

	@Override
	//định lý bắt tay:2 lần cạnh=tổng bậc các đỉnh
	public int numEdgesGraph() {

		int sum = 0;
		for (int i = 0; i < adj.length; i++) {

			sum += degreeVex(i);

		}
                                //Edg of ungraph
		System.out.println("So canh cua do thi: " + sum / 2);
		return sum / 2;
	}

	@Override
	public void printGraph() {
		System.out.println("Ma tran cua do thi\n");
		for (int i = 0; i < adj.length; i++) {
			for (int j = 0; j < adj[i].length; j++) {
				System.out.print(adj[i][j] + "    ");

			}
			System.out.println();
			System.out.println(adj.length);
		}
	}
}
