package Tuan_1;

public abstract  class Graph {
    public int numVex;
    public int [][] adj;

    public Graph(int v){
        this.numVex=v;
        this.adj= new int[numVex][numVex];
    }
    // Tuan 1
    // add edg
    public abstract void addEdges(int src,int dest);
    //remove edg
    public abstract void delEdges(int src,int dest);
    // deg of vex
    public  abstract  int degreeVex(int anyVex);
    // deg of graph
    public abstract int degreeGraph();
    //number of edg
    public abstract int numEdgesGraph();
    //print
    public abstract void printGraph();





}
