package Tuan_1;

public class TestDirGraph {
	public static void main(String[] args) {
		DirGraph dg = new DirGraph(7);
		dg.addEdges(1, 2);
		dg.addEdges(3, 2);
		dg.addEdges(3, 4);
		dg.addEdges(5, 4);
		dg.addEdges(5, 6);
                                dg.addEdges(7, 6);

		System.out.println("Bac: " + dg.degreeVex(0));
		System.out.println("--------------");
		dg.degreeGraph();
		System.out.println("--------------");
		dg.numEdgesGraph();
		System.out.println("--------------");
		dg.printGraph();
		System.out.println("--------------");
                               //after removing
		System.out.println("Sau khi xoa cung");

		dg.delEdges(3, 2);

		System.out.println("Deg: " + dg.degreeVex(0));
		System.out.println("--------------");
		dg.degreeGraph();
		System.out.println("--------------");
		dg.numEdgesGraph();
		System.out.println("--------------");
		dg.printGraph();
	}
}
