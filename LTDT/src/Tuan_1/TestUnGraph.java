﻿package Tuan_1;

public class TestUnGraph {
	public static void main(String[] args) {
                                //create ungraph with 7 vex
		UnGraph ug = new UnGraph(7);
		ug.addEdges(1, 2);
		ug.addEdges(2, 3);
		ug.addEdges(2, 5);
		ug.addEdges(3, 5);
		ug.addEdges(3, 4);
		ug.addEdges(5, 6);
                                //print
		System.out.println("Deg of graph: "+ug.degreeVex(1));
		System.out.println("------------");
		ug.degreeGraph();
		System.out.println("------------");
		ug.numEdgesGraph();
		System.out.println("------------");
		ug.printGraph();
		
		System.out.println("------------");
		//after removing
		System.out.println("Sau khi xóa cạnh---------");
		
		ug.delEdges(1, 2);
		System.out.println("Deg of graph: "+ug.degreeVex(1));
		System.out.println("------------");
		System.out.println(ug.degreeVex(3));
		System.out.println("------------");
		ug.degreeGraph();
		System.out.println("------------");
		ug.numEdgesGraph();
		System.out.println("------------");
		ug.printGraph();
		
	}

}
