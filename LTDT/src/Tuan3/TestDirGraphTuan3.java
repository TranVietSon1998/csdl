package Tuan3;

public class TestDirGraphTuan3 {
	public static void main(String[] args) {
		DirGraph dg= new DirGraph(4);
		dg.addEdges(1, 2);
		dg.addEdges(3, 2);
		dg.addEdges(3, 4);
		dg.addEdges(4, 1);
		dg.addEdges(4, 2);
		dg.addEdges(3, 1);
		dg.printGraph();
		DirGraph dg1= new DirGraph(3);
		dg1.addEdges(1, 2);
		dg1.addEdges(3, 2);
		dg1.addEdges(3, 1);
		dg1.printGraph();
		DirGraph dg2= new DirGraph(3);
		dg2.addEdges(1, 2);
		dg2.addEdges(2, 3);
		dg2.addEdges(3, 1);
		dg2.printGraph();
		System.out.println("Do thi la con cua do thi khac: " + dg.isSubGraph2(dg1, dg));
		System.out.println("Do thi la con cua do thi khac: " + dg.isSubGraph2(dg2, dg));
		
		dg.DFS(0);
		
		dg.BFS(0);
		dg.findPathLong(0, 6);

		dg.findPathShortest(0, 6);
		
	}

}
