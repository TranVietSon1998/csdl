package Tuan3;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

import Tuan3.Graph.Edges;

public class DirGraph extends Graph {
	public DirGraph(int v) {
		super(v);
	}

	@Override
	public void addEdges(int src, int dest) {
		if (src >= 0 && dest >= 0 && src < numVex && dest <= numVex) {
			adj[src - 1][dest - 1] = 1;

		}
	}

	@Override
	public void delEdges(int src, int dest) {
		if (src >= 0 && dest >= 0 && src < numVex && dest <= numVex) {
			adj[src - 1][dest - 1] = 0;

		}
	}

	@Override
	public int degreeVex(int anyVex) {
		int sum = tongNuaBacNgoaiMotDinh(anyVex) + tongNuaBacTrongMotDinh(anyVex);
		return sum;
	}

	public int tongNuaBacNgoaiMotDinh(int anyVex) {
		int sum = 0;
		if (anyVex >= 0 && anyVex < numVex) {
			for (int i = 0; i < adj.length; i++) {

				sum += adj[anyVex][i];
			}
		}
		return sum;
	}

	public int tongNuaBacTrongMotDinh(int anyVex) {
		int sum = 0;
		if (anyVex >= 0 && anyVex < numVex) {
			for (int i = 0; i < adj.length; i++) {

				sum += adj[i][anyVex];
			}
		}
		return sum;
	}

	public int tongNuaBacTrongDoThi() {
		int sum = 0;

		for (int i = 0; i < adj.length; i++) {

			sum += tongNuaBacTrongMotDinh(i);
		}

		return sum;
	}

	public int tongNuaBacNgoaiDoThi() {
		int sum = 0;

		for (int i = 0; i < adj.length; i++) {

			sum += tongNuaBacNgoaiMotDinh(i);
		}

		return sum;
	}

	@Override
	public int degreeGraph() {
		int sum = 0;
		for (int i = 0; i < adj.length; i++) {
			System.out.println("Bac cua dinh " + i + " la: " + degreeVex(i));
			sum += degreeVex(i);
		}
		System.out.println("Bac cua tat ca cac dinh " + sum);
		return sum;

	}

	@Override
// So cung cua do thi co huong = tong bac ngoai = tong bac trong
	public int numEdgesGraph() {
		int sum = 0;

		for (int i = 0; i < adj.length; i++) {

			sum += tongNuaBacNgoaiMotDinh(i);
		}
		System.out.println("So cung cua do thi la: " + sum);
		return sum;

	}

	@Override
	public void printGraph() {
		System.out.println("Ma tran cua do thi\n");
		for (int i = 0; i < adj.length; i++) {
			for (int j = 0; j < adj[i].length; j++) {
				System.out.print(adj[i][j] + "    ");

			}
			System.out.println();
		}
	}

	@Override
	public boolean isBipartiteGraph() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isSubGraph(Graph g1, Graph g2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void re_DFS(int startVex) {
		int V = startVex - 1;
		visited[V] = true;
		System.out.print((V + 1) + "\t");
		for (int i = 0; i < numVex; i++) {
			if (adj[V][i] != 0 && visited[i] == false) {
				re_DFS(i + 1);
			}
		}

	}

// Chuyen doi ma tran co huong ve ma tran vo huong
	public void chuyenDoiMaTran(DirGraph g) {
		int[][] test = g.adj;
		for (int i = 0; i < test.length; i++) {
			for (int j = 0; j < test.length; j++) {
				if (test[i][j] != 0) {
					this.adj[i][j] = test[i][j];
					this.adj[j][i] = test[j][i];

				}
			}
		}
	}

	// Kiem tra do thi lien thong yeu
	public boolean ktDoThiLienThongYeu() {
		chuyenDoiMaTran(this);
		int startVex=1;
		int V = startVex - 1;
		visited[V] = true;
		for (int i = 0; i < numVex; i++) {
			if (adj[V][i] != 0 && visited[i] == false) {
				re_DFS(i + 1);
			}
		}
		// check
		for (int i = 0; i < numVex; i++) {
			if (visited[i] == false)
				return false;
		}
		return true;

	}

	// Kiem tra do thi lien thong manh
	public boolean ktDoThiLienThongManh() {
		boolean right = true;
		int startVex=1;
		int V = startVex - 1;
		visited[V] = true;
		for (int i = 0; i < numVex; i++) {
			if (adj[V][i] != 0 && visited[i] == false) {
				re_DFS(i + 1);
			}
		}
		for (int k = 0; k < visited.length; k++) {
			if (tongNuaBacNgoaiMotDinh(k) == 0 || tongNuaBacTrongMotDinh(k) == 0)
				right = false;
			if (visited[k] != true)
				return false;

		}
		if (right) {
			return true;
		} else {
			return false;
		}

	}

	@Override
	public boolean isConnection() {
		if (ktDoThiLienThongManh() || ktDoThiLienThongYeu()) {
			return true;
		}

		return false;
	}

	@Override
	public int countConnection() {
		int demLienThong = 1;
		int i = 0;
		int numberPoint = adj.length;
		Stack<Integer> stack = new Stack<Integer>();
		ArrayList<Integer> listVisit = new ArrayList<>();
		// dau tien la gan setTemp bang tat ca cac dinh co trong do thi
		ArrayList<Integer> setTmp = new ArrayList<>();
		for (int j = 0; j < adj.length; j++) {
			setTmp.add(j);
		}
		int[] visit = new int[numberPoint];
		visit[i] = 1;
		listVisit.add(i);
		stack.push(i);
		setTmp.remove(i);
		while (!stack.empty()) {
			int count = 0;
			i = stack.peek();
			for (int j = 0; j < visit.length; j++) {
				if (this.adj[i][j] > 0 && visit[j] != 1) {
					listVisit.add(j);
					visit[j] = 1;
					stack.push(j);
					// neu phan tu do da duyet roi thi xoa di
					for (int j2 = 0; j2 < setTmp.size(); j2++) {
						if (setTmp.get(j2) == j) {
							setTmp.remove(j2);
						}
					}
					break;
				} else {
					count++;
				}
			}
			if (count == visit.length) {
				// neu phan tu duyet xong roi thi break
				if (!setTmp.isEmpty()) {
					// chua thi lay phan tu dau tien nhat ra duyet tiep
					i = setTmp.get(0);
					if (i < adj.length) {
						demLienThong++;
						stack.removeAll(stack);
						// thuc hien push de stack khong rong
						stack.push(i);
						// remove lo trinh cu de ghi vao lo trinh moi
						listVisit.removeAll(listVisit);
					} else {
						break;
					}
				} else {
					break;
				}
			}
		}
		return demLienThong;
	}

	@Override
	public void findConnection() {
		int demLienThong = 1;
		int i = 0;
		int numberPoint = adj.length;
		Stack<Integer> stack = new Stack<Integer>();
		ArrayList<Integer> listVisit = new ArrayList<>();
		// dau tien la gan setTemp bang tat ca cac dinh co trong do thi
		ArrayList<Integer> setTmp = new ArrayList<>();
		for (int j = 0; j < adj.length; j++) {
			setTmp.add(j);
		}
		int[] visit = new int[numberPoint];
		visit[i] = 1;
		listVisit.add(i);
		stack.push(i);
		setTmp.remove(i);
		while (!stack.empty()) {
			int count = 0;
			i = stack.peek();
			for (int j = 0; j < visit.length; j++) {
				if (this.adj[i][j] > 0 && visit[j] != 1) {
					listVisit.add(j);
					visit[j] = 1;
					stack.push(j);
					// neu phan tu do da duyet roi thi xoa di
					for (int j2 = 0; j2 < setTmp.size(); j2++) {
						if (setTmp.get(j2) == j) {
							setTmp.remove(j2);
						}
					}
					break;
				} else {
					count++;
				}
			}
			if (count == visit.length) {
				// neu khong con duong di
				System.out.println("So thanh phan lien thong : " + demLienThong);
				// xuat ra lo trinh
				print(listVisit);
				// neu phan tu duyet xong roi thi break
				if (!setTmp.isEmpty()) {
					// chua thi lay phan tu dau tien nhat ra duyet tiep
					i = setTmp.get(0);
					if (i < adj.length) {
						demLienThong++;
						stack.removeAll(stack);
						// thuc hien push de stack khong rong
						stack.push(i);
						// remove lo trinh cu de ghi vao lo trinh moi
						listVisit.removeAll(listVisit);
					} else {
						break;
					}
				} else {
					break;
				}
			}
		}

	}

//	private void print(ArrayList<Integer> listVisit) {
//		String string = "";
//		String temp = " ==> ";
//		for (int k = 0; k < listVisit.size(); k++) {
//			string += listVisit.get(k)+1;
//			if (k < listVisit.size() - 1) {
//				string += temp;
//			}
//		}
//		System.out.println(string);
//		
//	}

	@Override
	public boolean isSubGraph2(Graph g1, Graph g2) {
		ArrayList<Edges> listSub = new ArrayList<>();// con
		ArrayList<Edges> listSup = new ArrayList<>();// cha
		int count = 0;
		// Kiem tra dinh : dinh con <= dinh cha
        //g1 là con
		// g2 là cha
		if (g1.numVex <= g2.numVex) {
			for (int i = 0; i < g1.numVex; i++) {
				for (int j = 0; j < g1.numVex; j++) {
					if (g1.adj[i][j]!=0) {
						listSub.add(new Edges(i, j));//lấy tất cả các cạnh của con(thêm cạnh)
					}
				}
			}
		}

		// Them canh la listSup
		for (int i = 0; i < g2.numVex; i++) {
			for (int j = 0; j < g2.numVex; j++) {
				if (g2.adj[i][j] != 0) {
					listSup.add(new Edges(i, j));
				}
			}
		}

// Do thi con co chua trong do thi cha hay ko
		for (int i = 0; i < listSub.size(); i++) {
			for (int j = 0; j < listSup.size(); j++) {
				if (listSub.get(i).toString().equalsIgnoreCase(listSup.get(j).toString())) {
					count++;
				}
			}
		}
		if (count == listSub.size()) {
			return true;
		}
		return false;
	}

	@Override
	// Stack LIFO
		//đỉnh đã thăm =1,đỉnh chưa thăm =0;có cạnh =1 ,không cạnh =0
		public void DFS(int start) {
			//nếu chạy từ 1 đến 7 từ nó sẽ duyệt từ 0 đến 6....
			System.out.println("Duyet DFS tu dinh: " + (start + 1));
			Stack<Integer> stack = new Stack<>();
			// list de luu danh sach duong di da qua
			ArrayList<Integer> listVisit = new ArrayList<>();
			// arr danh dau dinh da qua
			int[] visit = new int[numVex];
			// mãng visit đánh dấu những đỉnh đã qua
			visit[start] = 1;//gán đỉnh đã qua bằng 1,chưa qua bằng 0
			listVisit.add(start);
			// Push bổ sung một phần tử vào đỉnh (top) của ngăn xếp,
			// nghĩa là sau các phần tử đã có trong ngăn xếp.
			// Pop giải phóng và trả về phần tử đang đứng ở đỉnh của ngăn xếp
			stack.push(start);
			// dừng khi stack rỗng
			while (!stack.empty()) {
				// bien count de kiem tra khi nao can lay phan tu ra khoi stack
				int count = 0;//dùng để xác định điểm dừng
				// peek kiểm tra xem đỉnh của danh sách có rỗng không
				// nếu rỗng thì trả về null còn không trả về giá trị của đỉnh danh sách.
				start = stack.peek();
				// duyet va kiem tra
				for (int j = 0; j < visit.length; j++) {
					//this.adj[start][j] > 0 ...có cạnh
					//visit[j] == 0 là đỉnh chưa đi qua
					if (this.adj[start][j] != 0 && visit[j] == 0) {
						// neu j chưa thăm thi them vao list
						listVisit.add(j);
						visit[j] = 1;//gán lại trạng thái đã thăm =1
						stack.push(j);
						// neu them roi thi dung lai theo quy tac cuon
						break;
					} else {
						count++;
					}
				}
				if (count == visit.length) {
					stack.pop();
				}
			}
			print(listVisit);
	}

		private void print(ArrayList<Integer> listVisit) {
			String string = "";
			String temp = " ==> ";
			for (int k = 0; k < listVisit.size(); k++) {
				string += listVisit.get(k) + 1;
				if (k < listVisit.size() - 1) {
					string += temp;
				}
			}
			System.out.println(string);

		}


	@Override
	public void BFS(int start) {
		System.out.println("Duyet BFS tu dinh: " + (start + 1));
		Queue<Integer> queue = new LinkedList<>();
		boolean[] visited = new boolean[numVex];
		ArrayList<Integer> list = new ArrayList<>();
		visited[start] = true;//qui định trạng thái..true==1,false=0
		list.add(start);
		queue.add(start);

		while (!queue.isEmpty()) {
			start = queue.remove();
			for (int i = 0; i < visited.length; i++) {
				if (this.adj[start][i] != 0 && visited[i] == false) {
					list.add(i);
					visited[i] = true;
					queue.add(i);

				}
			}

		}
		print(list);
	}

	@Override
	// Duyet theo chieu sau tim duong dai nhat
		public void findPathLong(int pointStart, int pointEnd) {
			System.out.println("Duong di dai nhat tu dinh: " + (pointStart + 1) + " den: " + (pointEnd + 1) + " la: ");

			// stack de phong ngua truong hop quay lui ;
			Stack<Integer> stack = new Stack<>();
			// list de luu danh sach duong di da qua
			ArrayList<Integer> listVisit = new ArrayList<>();
			// arr danh dau dinh da qua
			int[] visit = new int[numVex];
			// dau tien add dinh da cho vao
			visit[pointStart] = 1;
			listVisit.add(pointStart);
			// Push bổ sung một phần tử vào đỉnh (top) của ngăn xếp,
			// nghĩa là sau các phần tử đã có trong ngăn xếp.
			// Pop giải phóng và trả về phần tử đang đứng ở đỉnh của ngăn xếp
			stack.push(pointStart);
			// dừng khi stack rỗng
			while (!stack.empty()) {
				// bien count de kiem tra khi nao can lay phan tu ra khoi stack
				int count = 0;
				// peek kiểm tra xem đỉnh của danh sách có rỗng không
				// nếu rỗng thì trả về null còn không trả về giá trị của đỉnh danh sách.
				pointStart = stack.peek();
	//nếu điểm đầu trùng điểm cuối thì dừng
				if (pointStart == pointEnd) {
					break;
				}
				// duyet va kiem tra
				for (int j = 0; j < visit.length; j++) {
					if (this.adj[pointStart][j] != 0 && visit[j] == 0) {
						// neu j chưa thăm thi them vao list
						listVisit.add(j);
						visit[j] = 1;
						stack.push(j);
						// neu them roi thi dung lai theo quy tac cuon
						break;
					} else {
						count++;
					}
				}
				if (count == visit.length) {
					stack.pop();
				}
			}
			print(listVisit);

		}


	@Override
	public void findPathShortest(int pointStart, int pointEnd) {
		System.out.println("Duong di ngan nhat tu dinh: " + (pointStart + 1) + " den: " + (pointEnd + 1) + " la: ");
		Queue<Integer> queue = new LinkedList<>();
		// boolean[] visited = new boolean[numVex];
		ArrayList<Integer> list = new ArrayList<>();
		visited[pointStart] = true;
		list.add(pointStart);
		queue.add(pointStart);
		while (!queue.isEmpty()) {

			pointStart = queue.poll(); // ở đây, lấy từ đằng sau
			for (int i = 0; i < numVex; i++) {
				if (this.adj[pointStart][i] != 0 && visited[i] == false) {

					list.add(i);
					visited[i] = true;
					queue.add(i);
					if (i == pointEnd) {
						queue.clear(); // xóa hết yêu cầu dòng while dừng
						break;
					}

				}

			}
		}

//		}
		print(list);
	}
}
