package Tuan3;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class UnGraph extends Graph {
	String result;
	String s;

	public UnGraph(int v) {
		super(v);
	}

	@Override
	// Them canh
	public void addEdges(int src, int dest) {
		if (src >= 0 && dest >= 0 && src < numVex && dest <= numVex) {
			adj[src - 1][dest - 1] = 1;
			adj[dest - 1][src - 1] = 1;
		}
	}

	@Override
	// Xoa canh
	public void delEdges(int src, int dest) {
		if (src >= 0 && dest >= 0 && src < numVex && dest <= numVex) {
			adj[src - 1][dest - 1] = 0;
			adj[dest - 1][src - 1] = 0;
		}
	}

	@Override
	// Bac 1 dinh bat ky
	public int degreeVex(int anyVex) {
		int sum = 0;
		if (anyVex > -1 && anyVex < adj.length) {
			for (int i = 0; i < adj[anyVex].length; i++) {
				sum += adj[i][anyVex];
			}
			return sum;
		} else {
			return -1;
		}

	}

	@Override
// bac cua do thi
	public int degreeGraph() {
		int sum = 0;
		for (int i = 0; i < adj.length; i++) {
			System.out.println("Bac cua dinh " + i + " la: " + degreeVex(i));
			sum += degreeVex(i);

		}
		System.out.println("Bac cua tat ca cac dinh " + sum);
		return sum;
	}

	@Override
	// So canh cua 1 do thi
	public int numEdgesGraph() {

		int sum = 0;
		for (int i = 0; i < adj.length; i++) {

			sum += degreeVex(i);

		}
		System.out.println("So canh cua do thi: " + sum / 2);
		return sum / 2;
	}

	@Override
	// In do thi bang ma tran ke
	public void printGraph() {
		System.out.println("Ma tran cua do thi\n");
		for (int i = 0; i < adj.length; i++) {
			for (int j = 0; j < adj[i].length; j++) {
				System.out.print(adj[i][j] + "    ");

			}
			System.out.println();
		}
	}

	@Override
	// Kiem tra do thi co luong phan hay khogn
	public boolean isBipartiteGraph() {
		int[] color = new int[numVex];
		// -1 no color,1 color one, 0 color 2
		for (int i = 0; i < color.length; i++) {
			color[i] = -1;
		}

		// step 2 color
		for (int i = 0; i < color.length; i++) {
			for (int j = 0; j < color.length; j++) {
				if (adj[i][j] != 0 && color[j] == -1) {
					if (color[i] == 1)
						color[j] = 0;
					else
						color[j] = 1;
				}
			}
		}

		// step 3 check neu co 2 dinh ke nhau cung mau thi ko luong phan
		for (int i = 0; i < color.length; i++) {
			for (int j = 0; j < color.length; j++) {
				if (adj[i][j] != 0 && color[i] == color[j]) {
					return false;
				}
			}
		}
		return true;
	}

	// Kiem tra do thi nay co chua trong do thi khac hay khong ?
	// So luong canh = nhau ?
	// Canh cua A giong canh cua B ?
	public boolean isContain(ArrayList<Edges> arr1, ArrayList<Edges> arr2) {
		int count = 0;
		for (int i = 0; i < arr1.size(); i++) {
			for (int j = 0; j < arr2.size(); j++) {
				if (arr1.get(i).toString().equals(arr2.get(j).toString())) {
					count++;
				}
			}
		}
		if (count == arr2.size()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	// Kiem tra 1 do thi co phai la con cua do thi khac
	public boolean isSubGraph(Graph g1, Graph g2) {
		int countG1 = g1.adj.length;
		int countG2 = g2.adj.length;
		ArrayList<Edges> listCha = new ArrayList<>();
		ArrayList<Edges> listCon = new ArrayList<>();

		for (int i = 0; i < numVex; i++) {
			for (int j = 0; j < numVex; j++) {
				if (g2.adj[i][j] > 0)
					listCha.add(new Edges(i + 1, j + 1));

			}
		}
		System.out.println(listCha);

		for (int k = 0; k < g1.adj.length; k++) {

			for (int m = 0; m < g1.adj.length; m++) {

				if (g1.adj[k][m] > 0)
					listCon.add(new Edges(k + 1, m + 1));

			}
		}
		System.out.println(listCon);
		if (countG1 <= countG2 && isContain(listCha, listCon))

			return true;
		else
			return false;

	}

// Duyet do thi theo chieu sau dung de quy
	@Override
	public void re_DFS(int startVex) {

		int V = startVex - 1;
		visited[V] = true;
		System.out.print((V + 1) + "\t");
		for (int i = 0; i < numVex; i++) {
			if (adj[V][i] != 0 && visited[i] == false) {
				re_DFS(i + 1);
			}
		}

	}

	@Override
	// Kiem tra tinh lien thong cua do thi
	public boolean isConnection() {
		int startVex = 1;

		int V = startVex - 1;
		visited[V] = true;

		// check
		for (int i = 0; i < numVex; i++) {
			if (visited[i] == false)
				return false;
		}
		return true;
	}

	@Override
	// Dem so thanh phan lien thong
	public int countConnection() {
		int demLienThong = 1;
		int i = 0;
		int numberPoint = adj.length;
		Stack<Integer> stack = new Stack<Integer>();
		ArrayList<Integer> listVisit = new ArrayList<>();
		// dau tien la gan setTemp bang tat ca cac dinh co trong do thi
		ArrayList<Integer> setTmp = new ArrayList<>();
		for (int j = 0; j < adj.length; j++) {
			setTmp.add(j);
		}
		int[] visit = new int[numberPoint];
		visit[i] = 1;
		listVisit.add(i);
		stack.push(i);
		setTmp.remove(i);
		while (!stack.empty()) {
			int count = 0;
			i = stack.peek();
			for (int j = 0; j < visit.length; j++) {
				if (this.adj[i][j] > 0 && visit[j] != 1) {
					listVisit.add(j);
					visit[j] = 1;
					stack.push(j);
					// neu phan tu do da duyet roi thi xoa di
					for (int j2 = 0; j2 < setTmp.size(); j2++) {
						if (setTmp.get(j2) == j) {
							setTmp.remove(j2);
						}
					}
					break;
				} else {
					count++;
				}
			}
			if (count == visit.length) {
				// neu phan tu duyet xong roi thi break
				if (!setTmp.isEmpty()) {
					// chua thi lay phan tu dau tien nhat ra duyet tiep
					i = setTmp.get(0);
					if (i < adj.length) {
						demLienThong++;
						stack.removeAll(stack);
						// thuc hien push de stack khong rong
						stack.push(i);
						// remove lo trinh cu de ghi vao lo trinh moi
						listVisit.removeAll(listVisit);
					} else {
						break;
					}
				} else {
					break;
				}
			}
		}
		return demLienThong;

	}

	@Override
	// Tim cac thanh phan lien thong cua do thi
	public void findConnection() {
		int demLienThong = 1;
		int i = 0;
		int numberPoint = adj.length;
		Stack<Integer> stack = new Stack<Integer>();
		ArrayList<Integer> listVisit = new ArrayList<>();
		// dau tien la gan setTemp bang tat ca cac dinh co trong do thi
		ArrayList<Integer> setTmp = new ArrayList<>();
		for (int j = 0; j < adj.length; j++) {
			setTmp.add(j);
		}
		int[] visit = new int[numberPoint];
		visit[i] = 1;
		listVisit.add(i);
		stack.push(i);
		setTmp.remove(i);
		while (!stack.empty()) {
			int count = 0;
			i = stack.peek();
			for (int j = 0; j < visit.length; j++) {
				if (this.adj[i][j] > 0 && visit[j] != 1) {
					listVisit.add(j);
					visit[j] = 1;
					stack.push(j);
					// neu phan tu do da duyet roi thi xoa di
					for (int j2 = 0; j2 < setTmp.size(); j2++) {
						if (setTmp.get(j2) == j) {
							setTmp.remove(j2);
						}
					}
					break;
				} else {
					count++;
				}
			}
			if (count == visit.length) {
				// neu khong con duong di
				System.out.println("So thanh phan lien thong : " + demLienThong);
				// xuat ra lo trinh
				print(listVisit);
				// neu phan tu duyet xong roi thi break
				if (!setTmp.isEmpty()) {
					// chua thi lay phan tu dau tien nhat ra duyet tiep
					i = setTmp.get(0);
					if (i < adj.length) {
						demLienThong++;
						stack.removeAll(stack);
						// thuc hien push de stack khong rong
						stack.push(i);
						// remove lo trinh cu de ghi vao lo trinh moi
						listVisit.removeAll(listVisit);
					} else {
						break;
					}
				} else {
					break;
				}
			}
		}

	}
//tuan 3

	@Override
	public boolean isSubGraph2(Graph g1, Graph g2) {
		ArrayList<Edges> listSub = new ArrayList<>();// con
		ArrayList<Edges> listSup = new ArrayList<>();// cha
		int count = 0;
		// Kiem tra dinh : dinh con <= dinh cha
        //g1 là con
		// g2 là cha
		if (g1.numVex <= g2.numVex) {
			for (int i = 0; i < g1.numVex; i++) {
				for (int j = 0; j < g1.numVex; j++) {
					if (g1.adj[i][j]!=0) {
						listSub.add(new Edges(i, j));//lấy tất cả các cạnh của con(thêm cạnh)
					}
				}
			}
		}

		// Them canh la listSup
		for (int i = 0; i < g2.numVex; i++) {
			for (int j = 0; j < g2.numVex; j++) {
				if (g2.adj[i][j] != 0) {
					listSup.add(new Edges(i, j));
				}
			}
		}

// Do thi con co chua trong do thi cha hay ko
		for (int i = 0; i < listSub.size(); i++) {
			for (int j = 0; j < listSup.size(); j++) {
				if (listSub.get(i).toString().equalsIgnoreCase(listSup.get(j).toString())) {
					count++;
				}
			}
		}
		if (count == listSub.size()) {
			return true;
		}
		return false;
	}

	@Override
	// Stack LIFO
	//đỉnh đã thăm =1,đỉnh chưa thăm =0;có cạnh =1 ,không cạnh =0
	public void DFS(int start) {
		//nếu chạy từ 1 đến 7 từ nó sẽ duyệt từ 0 đến 6....
		System.out.println("Duyet DFS tu dinh: " + (start + 1));
		Stack<Integer> stack = new Stack<>();
		// list de luu danh sach duong di da qua
		ArrayList<Integer> listVisit = new ArrayList<>();
		// arr danh dau dinh da qua
		int[] visit = new int[numVex];
		// mãng visit đánh dấu những đỉnh đã qua
		visit[start] = 1;//gán đỉnh đã qua bằng 1,chưa qua bằng 0
		listVisit.add(start);
		// Push bổ sung một phần tử vào đỉnh (top) của ngăn xếp,
		// nghĩa là sau các phần tử đã có trong ngăn xếp.
		// Pop giải phóng và trả về phần tử đang đứng ở đỉnh của ngăn xếp
		stack.push(start);
		// dừng khi stack rỗng
		while (!stack.empty()) {
			// bien count de kiem tra khi nao can lay phan tu ra khoi stack
			int count = 0;//dùng để xác định điểm dừng
			// peek kiểm tra xem đỉnh của danh sách có rỗng không
			// nếu rỗng thì trả về null còn không trả về giá trị của đỉnh danh sách.
			start = stack.peek();
			// duyet va kiem tra
			for (int j = 0; j < visit.length; j++) {
				//this.adj[start][j] > 0 ...có cạnh
				//visit[j] == 0 là đỉnh chưa đi qua
				if (this.adj[start][j] > 0 && visit[j] == 0) {
					// neu j chưa thăm thi them vao list
					listVisit.add(j);
					visit[j] = 1;//gán lại trạng thái đã thăm =1
					stack.push(j);
					// neu them roi thi dung lai theo quy tac cuon
					break;
				} else {
					count++;
				}
			}
			if (count == visit.length) {
				stack.pop();
			}
		}
		print(listVisit);
	}

	private void print(ArrayList<Integer> listVisit) {
		String string = "";
		String temp = " ==> ";
		for (int k = 0; k < listVisit.size(); k++) {
			string += listVisit.get(k) + 1;
			if (k < listVisit.size() - 1) {
				string += temp;
			}
		}
		System.out.println(string);

	}

	@Override
	public void BFS(int start) {
		System.out.println("Duyet BFS tu dinh: " + (start + 1));
		Queue<Integer> queue = new LinkedList<>();
		boolean[] visited = new boolean[numVex];
		ArrayList<Integer> list = new ArrayList<>();
		visited[start] = true;//qui định trạng thái..true==1,false=0
		list.add(start);
		queue.add(start);

		while (!queue.isEmpty()) {
			start = queue.remove();
			for (int i = 0; i < visited.length; i++) {
				if (this.adj[start][i] > 0 && visited[i] == false) {
					list.add(i);
					visited[i] = true;
					queue.add(i);

				}
			}

		}
		print(list);
	}

	@Override
	// Duyet theo chieu sau tim duong dai nhat
	public void findPathLong(int pointStart, int pointEnd) {
		System.out.println("Duong di dai nhat tu dinh: " + (pointStart + 1) + " den: " + (pointEnd + 1) + " la: ");

		// stack de phong ngua truong hop quay lui ;
		Stack<Integer> stack = new Stack<>();
		// list de luu danh sach duong di da qua
		ArrayList<Integer> listVisit = new ArrayList<>();
		// arr danh dau dinh da qua
		int[] visit = new int[numVex];
		// dau tien add dinh da cho vao
		visit[pointStart] = 1;
		listVisit.add(pointStart);
		// Push bổ sung một phần tử vào đỉnh (top) của ngăn xếp,
		// nghĩa là sau các phần tử đã có trong ngăn xếp.
		// Pop giải phóng và trả về phần tử đang đứng ở đỉnh của ngăn xếp
		stack.push(pointStart);
		// dừng khi stack rỗng
		while (!stack.empty()) {
			// bien count de kiem tra khi nao can lay phan tu ra khoi stack
			int count = 0;
			// peek kiểm tra xem đỉnh của danh sách có rỗng không
			// nếu rỗng thì trả về null còn không trả về giá trị của đỉnh danh sách.
			pointStart = stack.peek();
//nếu điểm đầu trùng điểm cuối thì dừng
			if (pointStart == pointEnd) {
				break;
			}
			// duyet va kiem tra
			for (int j = 0; j < visit.length; j++) {
				if (this.adj[pointStart][j] > 0 && visit[j] == 0) {
					// neu j chưa thăm thi them vao list
					listVisit.add(j);
					visit[j] = 1;
					stack.push(j);
					// neu them roi thi dung lai theo quy tac cuon
					break;
				} else {
					count++;
				}
			}
			if (count == visit.length) {
				stack.pop();
			}
		}
		print(listVisit);

	}

	@Override

	public void findPathShortest(int pointStart, int pointEnd) {
		System.out.println("Duong di ngan nhat tu dinh: " + (pointStart + 1) + " den: " + (pointEnd + 1) + " la: ");
		Queue<Integer> queue = new LinkedList<>();
		// boolean[] visited = new boolean[numVex];
		ArrayList<Integer> list = new ArrayList<>();
		visited[pointStart] = true;
		list.add(pointStart);
		queue.add(pointStart);
		while (!queue.isEmpty()) {

			pointStart = queue.poll(); // ở đây, lấy từ đằng sau
			for (int i = 0; i < numVex; i++) {
				if (this.adj[pointStart][i] > 0 && visited[i] == false) {

					list.add(i);
					visited[i] = true;
					queue.add(i);
					if (i == pointEnd) {
						queue.clear(); // xóa hết yêu cầu dòng while dừng
						break;
					}

				}

			}
		}

//		}
		print(list);
	}
}
