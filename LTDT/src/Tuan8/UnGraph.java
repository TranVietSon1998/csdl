package Tuan8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

public class UnGraph extends Graph {
	String result;

	public UnGraph(int v) {
		super(v);
	}

	// =========================Bai Tap Tuan1============================

	@Override
	// Them canh
	public void addEdges(int src, int dest) {
		if (src >= 0 && dest >= 0 && src <= numVex && dest <= numVex) {
			adj[src - 1][dest - 1] = 1;
			adj[dest - 1][src - 1] = 1;
		}
	}

	// Them canh co trong so

	public void addEdges(int src, int dest, int weight) {
		if (src >= 0 && dest >= 0 && src <= numVex && dest <= numVex) {
			adj[src - 1][dest - 1] = weight;
			adj[dest - 1][src - 1] = weight;
		}
	}

	@Override
	// Xoa canh
	public void delEdges(int src, int dest) {
		if (src >= 0 && dest >= 0 && src <= numVex && dest <= numVex) {
			adj[src - 1][dest - 1] = 0;
			adj[dest - 1][src - 1] = 0;
		}
	}

	@Override
	// Bac 1 dinh bat ky
	public int degreeVex(int anyVex) {
		int sum = 0;
		if (anyVex >= 0 && anyVex < adj.length) {
			for (int i = 0; i < adj[anyVex].length; i++) {
				sum += adj[i][anyVex];
			}
			return sum;
		} else {
			return -1;
		}
	}

	@Override
// bac cua do thi
	public int degreeGraph() {
		int sum = 0;
		for (int i = 0; i < adj.length; i++) {
			System.out.println("Bac cua dinh " + i + " la: " + degreeVex(i));
			sum += degreeVex(i);
		}
		System.out.println("Bac cua tat ca cac dinh " + sum);
		return sum;
	}

	@Override
	// So canh cua 1 do thi
	public int numEdgesGraph() {
		int sum = 0;
		for (int i = 0; i < adj.length; i++) {
			sum += degreeVex(i);
		}
		System.out.println("So canh cua do thi: " + sum / 2);
		return sum / 2;
	}

	@Override
	// In do thi bang ma tran ke
	public void printGraph() {
		System.out.println("Ma tran cua do thi\n");
		for (int i = 0; i < adj.length; i++) {
			for (int j = 0; j < adj[i].length; j++) {
				System.out.print(adj[i][j] + "    ");
			}
			System.out.println();
		}
	}

	// =========================Bai Tap Tuan2============================

	@Override
	// Kiem tra do thi co luong phan hay khogn
	public boolean isBipartiteGraph() {
		int[] color = new int[numVex];
		// -1 no color,1 color one, 0 color 2
		for (int i = 0; i < color.length; i++) {
			color[i] = -1;
		}

		// step 2 color
		for (int i = 0; i < color.length; i++) {
			for (int j = 0; j < color.length; j++) {
				if (adj[i][j] != 0 && color[j] == -1) {
					if (color[i] == 1)
						color[j] = 0;
					else
						color[j] = 1;
				}
			}
		}

		// step 3 check neu co 2 dinh ke nhau cung mau thi ko luong phan
		for (int i = 0; i < color.length; i++) {
			for (int j = 0; j < color.length; j++) {
				if (adj[i][j] != 0 && color[i] == color[j]) {
					return false;
				}
			}
		}
		return true;
	}

	// Kiem tra do thi nay co chua trong do thi khac hay khong ?
	// So luong canh = nhau ?
	// Canh cua A giong canh cua B ?
	public boolean isContain(ArrayList<Edges> arr1, ArrayList<Edges> arr2) {
		int count = 0;
		for (int i = 0; i < arr1.size(); i++) {
			for (int j = 0; j < arr2.size(); j++) {
				if (arr1.get(i).toString().equals(arr2.get(j).toString())) {
					count++;
				}
			}
		}
		if (count == arr2.size()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	// Kiem tra 1 do thi co phai la con cua do thi khac
	public boolean isSubGraph(Graph g1, Graph g2) {
		int countG1 = g1.adj.length;
		int countG2 = g2.adj.length;
		ArrayList<Edges> listCha = new ArrayList<>();
		ArrayList<Edges> listCon = new ArrayList<>();

		for (int i = 0; i < numVex; i++) {
			for (int j = 0; j < numVex; j++) {
				if (g2.adj[i][j] > 0)
					listCha.add(new Edges(i + 1, j + 1));

			}
		}
		System.out.println(listCha);

		for (int k = 0; k < g1.adj.length; k++) {

			for (int m = 0; m < g1.adj.length; m++) {

				if (g1.adj[k][m] > 0)
					listCon.add(new Edges(k + 1, m + 1));

			}
		}
		System.out.println(listCon);
		if (countG1 <= countG2 && isContain(listCha, listCon))

			return true;
		else
			return false;

	}

// Duyet do thi theo chieu sau dung de quy
	@Override
	public void re_DFS(int startVex) {

		int V = startVex - 1;
		visited[V] = true;
		System.out.print((V + 1) + "\t");
		for (int i = 0; i < numVex; i++) {
			if (adj[V][i] != 0 && visited[i] == false) {
				re_DFS(i + 1);
			}
		}

	}

	public void re_DFS2(int startVex) {

		int V = startVex - 1;
		visited[V] = true;

		for (int i = 0; i < numVex; i++) {
			if (adj[V][i] != 0 && visited[i] == false) {
				re_DFS2(i + 1);
			}
		}

	}

	@Override
	// Kiem tra tinh lien thong cua do thi
	public boolean isConnection() {

		re_DFS2(1);

		// check
		for (int i = 0; i < numVex; i++) {
			if (visited[i] == false)
				return false;
		}
		return true;
	}

	@Override
	// Dem so thanh phan lien thong
	public int countConnection() {
		int demLienThong = 1;
		int i = 0;
		int numberPoint = adj.length;
		Stack<Integer> stack = new Stack<Integer>();
		ArrayList<Integer> listVisit = new ArrayList<>();
		// dau tien la gan setTemp bang tat ca cac dinh co trong do thi
		ArrayList<Integer> setTmp = new ArrayList<>();
		for (int j = 0; j < adj.length; j++) {
			setTmp.add(j);
		}
		int[] visit = new int[numberPoint];
		visit[i] = 1;
		listVisit.add(i);
		stack.push(i);
		setTmp.remove(i);
		while (!stack.empty()) {
			int count = 0;
			i = stack.peek();
			for (int j = 0; j < visit.length; j++) {
				if (this.adj[i][j] > 0 && visit[j] != 1) {
					listVisit.add(j);
					visit[j] = 1;
					stack.push(j);
					// neu phan tu do da duyet roi thi xoa di
					for (int j2 = 0; j2 < setTmp.size(); j2++) {
						if (setTmp.get(j2) == j) {
							setTmp.remove(j2);
						}
					}
					break;
				} else {
					count++;
				}
			}
			if (count == visit.length) {
				// neu phan tu duyet xong roi thi break
				if (!setTmp.isEmpty()) {
					// chua thi lay phan tu dau tien nhat ra duyet tiep
					i = setTmp.get(0);
					if (i < adj.length) {
						demLienThong++;
						stack.removeAll(stack);
						// thuc hien push de stack khong rong
						stack.push(i);
						// remove lo trinh cu de ghi vao lo trinh moi
						listVisit.removeAll(listVisit);
					} else {
						break;
					}
				} else {
					break;
				}
			}
		}
		return demLienThong;

	}

	@Override
	// Tim cac thanh phan lien thong cua do thi
	public void findConnection() {
		int demLienThong = 1;
		int i = 0;
		int numberPoint = adj.length;
		Stack<Integer> stack = new Stack<Integer>();
		ArrayList<Integer> listVisit = new ArrayList<>();
		// dau tien la gan setTemp bang tat ca cac dinh co trong do thi
		ArrayList<Integer> setTmp = new ArrayList<>();
		for (int j = 0; j < adj.length; j++) {
			setTmp.add(j);
		}
		int[] visit = new int[numberPoint];
		visit[i] = 1;
		listVisit.add(i);
		stack.push(i);
		setTmp.remove(i);
		while (!stack.empty()) {
			int count = 0;
			i = stack.peek();
			for (int j = 0; j < visit.length; j++) {
				if (this.adj[i][j] > 0 && visit[j] != 1) {
					listVisit.add(j);
					visit[j] = 1;
					stack.push(j);
					// neu phan tu do da duyet roi thi xoa di
					for (int j2 = 0; j2 < setTmp.size(); j2++) {
						if (setTmp.get(j2) == j) {
							setTmp.remove(j2);
						}
					}
					break;
				} else {
					count++;
				}
			}
			if (count == visit.length) {
				// neu khong con duong di
				System.out.println("So thanh phan lien thong : " + demLienThong);
				// xuat ra lo trinh
				print(listVisit);
				// neu phan tu duyet xong roi thi break
				if (!setTmp.isEmpty()) {
					// chua thi lay phan tu dau tien nhat ra duyet tiep
					i = setTmp.get(0);
					if (i < adj.length) {
						demLienThong++;
						stack.removeAll(stack);
						// thuc hien push de stack khong rong
						stack.push(i);
						// remove lo trinh cu de ghi vao lo trinh moi
						listVisit.removeAll(listVisit);
					} else {
						break;
					}
				} else {
					break;
				}
			}
		}

	}

	// =========================Bai Tap Tuan3============================

	@Override
	public boolean isSubGraph2(Graph g1, Graph g2) {
		ArrayList<Edges> listSub = new ArrayList<>();
		ArrayList<Edges> listSup = new ArrayList<>();
		int count = 0;
		// Kiem tra dinh : dinh con <= dinh cha

		if (g1.numVex <= g2.numVex) {
			for (int i = 0; i < g1.numVex; i++) {
				for (int j = 0; j < g1.numVex; j++) {
					if (g1.adj[i][j] != 0) {
						listSub.add(new Edges(i, j));
					}
				}
			}
		}

		// Them canh la listSup
		for (int i = 0; i < g2.numVex; i++) {
			for (int j = 0; j < g2.numVex; j++) {
				if (g2.adj[i][j] != 0) {
					listSup.add(new Edges(i, j));
				}
			}
		}

// Do thi con co chua trong do thi cha hay ko
		for (int i = 0; i < listSub.size(); i++) {
			for (int j = 0; j < listSup.size(); j++) {
				if (listSub.get(i).toString().equalsIgnoreCase(listSup.get(j).toString())) {
					count++;
				}
			}
		}
		if (count == listSub.size()) {
			return true;
		}
		return false;
	}

	@Override
	// Stack LIFO
	public void DFS(int start) {
		System.out.println("Duyet DFS tu dinh: " + (start + 1));
		// stack de phong ngua truong hop quay lui ;
		Stack<Integer> stack = new Stack<>();
		// list de luu danh sach duong di da qua
		ArrayList<Integer> listVisit = new ArrayList<>();
		// arr danh dau dinh da qua
		int[] visit = new int[numVex];
		// dau tien add dinh da cho vao
		visit[start] = 1;
		listVisit.add(start);
		// Push bổ sung một phần tử vào đỉnh (top) của ngăn xếp,
		// nghĩa là sau các phần tử đã có trong ngăn xếp.
		// Pop giải phóng và trả về phần tử đang đứng ở đỉnh của ngăn xếp
		stack.push(start);
		// dừng khi stack rỗng
		while (!stack.empty()) {
			// bien count de kiem tra khi nao can lay phan tu ra khoi stack
			int count = 0;
			// peek kiểm tra xem đỉnh của danh sách có rỗng không
			// nếu rỗng thì trả về null còn không trả về giá trị của đỉnh danh sách.
			start = stack.peek();
			// duyet va kiem tra
			for (int j = 0; j < visit.length; j++) {
				if (this.adj[start][j] > 0 && visit[j] == 0) {
					// neu j chưa thăm thi them vao list
					listVisit.add(j);
					visit[j] = 1;
					stack.push(j);
					// neu them roi thi dung lai theo quy tac cuon
					break;
				} else {
					count++;
				}
			}
			if (count == visit.length) {
				stack.pop();
			}
		}
		print(listVisit);
	}

	private void print(ArrayList<Integer> listVisit) {
		String string = "";
		String temp = " ==> ";
		for (int k = 0; k < listVisit.size(); k++) {
			string += listVisit.get(k) + 1;
			if (k < listVisit.size() - 1) {
				string += temp;
			}
		}
		System.out.println(string);

	}

	@Override
	public void BFS(int start) {
		System.out.println("Duyet BFS tu dinh: " + (start + 1));
		Queue<Integer> queue = new LinkedList<>();
		// boolean[] visited = new boolean[numVex];
		ArrayList<Integer> list = new ArrayList<>();
		visited[start] = true;
		list.add(start);
		queue.add(start);

		while (!queue.isEmpty()) {
			start = queue.remove();
			for (int i = 0; i < visited.length; i++) {
				if (this.adj[start][i] > 0 && visited[i] == false) {
					list.add(i);
					visited[i] = true;
					queue.add(i);
				}
			}
		}
		print(list);
	}

	@Override
	// Duyet theo chieu sau tim duong dai nhat
	public void findPathLong(int src, int dest) {
		System.out.println("Duong di dai nhat tu dinh: " + (src + 1) + " den: " + (dest + 1) + " la: ");

		// stack de phong ngua truong hop quay lui ;
		Stack<Integer> stack = new Stack<>();
		// list de luu danh sach duong di da qua
		ArrayList<Integer> listVisit = new ArrayList<>();
		// arr danh dau dinh da qua
		int[] visit = new int[numVex];
		// dau tien add dinh da cho vao
		visit[src] = 1;
		listVisit.add(src);
		// Push bổ sung một phần tử vào đỉnh (top) của ngăn xếp,
		// nghĩa là sau các phần tử đã có trong ngăn xếp.
		// Pop giải phóng và trả về phần tử đang đứng ở đỉnh của ngăn xếp
		stack.push(src);
		// dừng khi stack rỗng
		while (!stack.empty()) {
			// bien count de kiem tra khi nao can lay phan tu ra khoi stack
			int count = 0;
			// peek kiểm tra xem đỉnh của danh sách có rỗng không
			// nếu rỗng thì trả về null còn không trả về giá trị của đỉnh danh sách.
			src = stack.peek();

			if (src == dest) {
				break;
			}
			// duyet va kiem tra
			for (int j = 0; j < visit.length; j++) {
				if (this.adj[src][j] > 0 && visit[j] == 0) {
					// neu j chưa thăm thi them vao list
					listVisit.add(j);
					visit[j] = 1;
					stack.push(j);
					// neu them roi thi dung lai theo quy tac cuon
					break;
				} else {
					count++;
				}
			}
			if (count == visit.length) {
				stack.pop();
			}
		}
		print(listVisit);

	}

	@Override
//Tim duong di ngan nhat = BFS
	public void findPathShortest(int src, int dest) {
		System.out.println("Duong di ngan nhat tu dinh: " + (src + 1) + " den: " + (dest + 1) + " la: ");
		Queue<Integer> queue = new LinkedList<>();
		ArrayList<Integer> list = new ArrayList<>();
		visited[src] = true;
		list.add(src);
		queue.add(src);
		while (!queue.isEmpty()) {

			src = queue.poll(); // ở đây, lấy từ đằng sau
			for (int i = 0; i < numVex; i++) {
				if (this.adj[src][i] > 0 && visited[i] == false) {

					list.add(i);
					visited[i] = true;
					queue.add(i);
					if (i == dest) {
						queue.clear(); // xóa hết bắt dòng while dừng
						break;
					}

				}

			}
		}
		print(list);
	}

	// =========================Bai Tap Tuan4============================

	@Override
	public void findWay(int src, int dest) {
		System.out.println("Duong di dai nhat tu dinh: " + (src + 1) + " den: " + (dest + 1) + " la: ");

		// stack de phong ngua truong hop quay lui ;
		Stack<Integer> stack = new Stack<>();
		// list de luu danh sach duong di da qua
		ArrayList<Integer> listVisit = new ArrayList<>();
		// arr danh dau dinh da qua
		int[] visit = new int[numVex];
		// dau tien add dinh da cho vao
		visit[src] = 1;
		listVisit.add(src);
		// Push bổ sung một phần tử vào đỉnh (top) của ngăn xếp,
		// nghĩa là sau các phần tử đã có trong ngăn xếp.
		// Pop giải phóng và trả về phần tử đang đứng ở đỉnh của ngăn xếp
		stack.push(src);
		// dừng khi stack rỗng
		while (!stack.empty()) {
			// bien count de kiem tra khi nao can lay phan tu ra khoi stack
			int count = 0;
			// peek kiểm tra xem đỉnh của danh sách có rỗng không
			// nếu rỗng thì trả về null còn không trả về giá trị của đỉnh danh sách.
			src = stack.peek();

			if (src == dest) {
				break;
			}
			// duyet va kiem tra
			for (int j = 0; j < visit.length; j++) {
				if (this.adj[src][j] > 0 && visit[j] == 0) {
					// neu j chưa thăm thi them vao list
					listVisit.add(j);
					visit[j] = 1;
					stack.push(j);
					// neu them roi thi dung lai theo quy tac cuon
					break;
				} else {
					count++;
				}
			}
			if (count == visit.length) {
				stack.pop();
			}
		}
		print(listVisit);
	}

	@Override
	public void findWayShortest(int src, int dest) {
		System.out.println("Duong di ngan nhat tu dinh: " + (src + 1) + " den: " + (dest + 1) + " la: ");
		Queue<Integer> queue = new LinkedList<>();
		ArrayList<Integer> list = new ArrayList<>();
		visited[src] = true;
		list.add(src);
		queue.add(src);
		while (!queue.isEmpty()) {

			src = queue.poll(); // ở đây, lấy từ đằng sau
			for (int i = 0; i < numVex; i++) {
				if (this.adj[src][i] > 0 && visited[i] == false) {

					list.add(i);
					visited[i] = true;
					queue.add(i);
					if (i == dest) {
						queue.clear(); // xóa hết bắt dòng while dừng
						break;
					}

				}

			}
		}
		print(list);

	}

	@Override
	public boolean checkCycleEuler() {
		boolean result = false;
		int count = 0;

		for (int i = 0; i < numVex; i++) {
			if (degreeVex(i) % 2 == 0) {
				count++; // dem so dinh bac chan
			}

		}
// Mot chu trinh euler co so dinh >1, lien thong manh, cac dinh deu la bac chan
		if (isConnection() == true && numVex > 1 && count == numVex) {
			result = true;
		}
		return result;
	}

	@Override
	public boolean checkPathEuler() {
		boolean result = false;
		int count = 0;

		for (int i = 0; i < numVex; i++) {
			if (degreeVex(i) % 2 != 0)
				count++;

		}
		if (isConnection() == true && numVex > 1 && count == 2) // Duong di euler co dung 2 dinh bac le,lien thong manh,
																// va co so dinh >1
			result = true;

		return result;
	}

	@Override
	public void findCycleEuler(int start) {
		if (checkCycleEuler() == false) {
			System.out.println("Khong co chu trinh euler");
		} else {
			System.out.println("Chu trinh Euler la: ");
			Stack<Integer> stack = new Stack<Integer>();
			ArrayList<Integer> list = new ArrayList<Integer>(); // luu dinh da duyet

			stack.push(start);
			list.add(start);
			while (!stack.isEmpty()) {
				int x = stack.pop(); // lay ra

				for (int i = 0; i < numVex; i++) {
					if (adj[x][i] != 0) { // Co the lap lai dinh nhung khong lap lai canh
						stack.push(i);
						list.add(i);
						adj[x][i] = adj[i][x] = 0;
						x = i;
						i = 0;
					}
					if (i == numVex && !stack.isEmpty()) { // chay het cac canh
						stack.pop();
					}
				}

			}
			print(list);
		}
	}

	@Override
	public void findPathEuler(int start) {
		ArrayList<Integer> listHasPathEuler = new ArrayList<>();
		if (checkPathEuler() == false) {
			System.out.println("Khong co duong di Euler");
		} else {
			if (degreeVex(start) % 2 == 0) {//
				for (int i = 0; i < numVex; i++) {
					if (degreeVex(i) % 2 != 0) {
						listHasPathEuler.add(i);
					}
				}
				System.out.println("Khong co duong di tu " + start);
				System.out.println("Danh sach diem co duong di: " + listHasPathEuler.toString());
			} else {

				Stack<Integer> stack = new Stack<Integer>();
				ArrayList<Integer> list = new ArrayList<Integer>(); // luu dinh da duyet
				stack.push(start);
				list.add(start);

				while (!stack.isEmpty()) {
					int x = stack.pop(); // lay ra

					for (int j = 0; j < numVex; j++) {
						if (adj[x][j] != 0) { // Co the lap lai dinh nhung khong lap lai canh
							stack.push(j);
							list.add(j);
							adj[x][j] = adj[j][x] = 0;// xoa canh
							x = j;
							j = 0;
						}
						if (j == numVex && !stack.isEmpty()) { // chay het cac canh
							stack.pop();
						}
					}

				}
				print(list);
			}

		}
	}
	// =========================Bai Tap Tuan5============================
	// Bai tap tuan 5

	// kiem tra chu trinh hamilton
	@Override
	/*
	 * Đồ thị đủ luôn là đồ thị Hamilton. Với n lẻ và n ≥ 3 thì Kn có (n-1)/2 chu
	 * trình Hamilton đôi một không có cạnh chung. Giả sử G là đồ thị phân đôi với
	 * hai tập đỉnh X1, X2 và |X1| = |X2| = n. Nếu d(x) ≥ n/2 với mọi đỉnh x của G
	 * thì G là đồ thị Hamilton. Giả sử G là đồ thị vô hướng đơn gồm n đỉnh với n ≥3
	 * . Nếu d(x) ≥ n/2 với mọi đỉnh x của G thì G là đồ thị Hamilton. Giả sử G là
	 * đồ thị vô hướng đơn gồm n đỉnh với n ≥ 3. Nếu d(x) ≥ (n-1)/2 với mọi đỉnh x
	 * của G thì G có dây chuyền Hamilton. Giả sử G là đồ thị vô hướng đơn gồm n
	 * đỉnh với n ≥ 3. Nếu d(x) + d(y) ≥ n với mọi cặp đỉnh x,y không kề nhau của G
	 * thì G là đồ thị Hamilton. Giả sử G là đồ thị vô hướng đơn gồm n đỉnh và m
	 * cạnh. Nếu m ≥ { (n^{2}-3n+6)/2} { (n^{2}-3n+6)/2} thì G là đồ thị Hamilton.
	 */

	// cach 1: chu trinh hamilton phai lien thong + kt tung do thi
	public boolean checkCycleHamilton() {
		boolean result = false;
		if (this.isConnection()) {// 1 do thi co chu trinh hamilton phai lien thong
			int count1 = 0; // số đỉnh có bậc >= n/2
			int count2 = 0; // số đỉnh có bậc 2 (trường đồ thị là đồ thi vòng)
			int count3 = 0; // số đỉnh có bậc = k -1 (trường hợp là đồ thị đủ)

			for (int i = 0; i < numVex; i++) {
				if (degreeVex(i) >= numVex / 2)
					count1++;
				if (degreeVex(i) == 2)
					count2++;
				if (degreeVex(i) == numVex - 1)
					count3++;
			}
			if (numVex >= 3) {
				if (count1 == numVex || count2 == numVex || count3 == numVex)
					result = true;
			}
		}
		return result;

	}

	// kiem tra duong di hamilton
	@Override
	public boolean checkPathHamilton() {
		boolean result = false;

		/*
		 * Đồ thị có đương đi Hamilton khi : TH1 : Đồ thị có tất cả các đỉnh đểu có
		 * bậc>= (N-1)/2 TH2 : Đồ thị có đúng 2 đỉnh bậc 1, các đỉnh còn lại đều có bậc
		 * >=(N-1)/2 TH3 : Đồ thị có đúng 2 đỉnh bậc 1 và có số đỉnh < (N-1)/2 nhỏ hơn
		 * hoặc bằng số đỉnh có bậc 1
		 */
		if (this.isConnection()) {
			int count1 = 0; // số đỉnh có bậc >= (n-1)/2
			int count2 = 0; // số đỉnh có bậc = 1;
			int count3 = 0; // số đỉnh có bậc < (n-1)/2

			for (int i = 0; i < numVex; i++) {
				if (degreeVex(i) >= (numVex - 1) / 2)
					count1++;
				if (degreeVex(i) == 1)
					count2++;
				if (degreeVex(i) < (numVex - 1) / 2 && degreeVex(i) > 1)
					count3++;
			}
			if (numVex >= 3) {
				if ((count1 == numVex) || (count2 == 2 && count1 == numVex - 2) || (count2 == 2 && count3 <= count2))
					result = true;
			}

		}

		return result;
	}

	// In ra 1 chu trinh hamilton

	int[] hc = new int[numVex];

	public void findCycleHamilton() {

		for (int i = 0; i < adj.length; i++)
			visited[i] = false;
		hc[0] = 0;
		visited[0] = true;
		Expand(1);

	}

	private void Expand(int i) {
		for (int j = 0; j < numVex; j++)
			if (!visited[j] && this.adj[hc[i - 1]][j] > 0) {
				hc[i] = j;
				if (i < numVex - 1) {
					visited[j] = true;
					Expand(i + 1);
					visited[j] = false;
				} else if (this.adj[hc[i]][0] > 0) {
					printHamiltonCycle(hc);
				}
			}

	}

	private void printHamiltonCycle(int[] hc2) {
		System.out.println("Chu trinh hamilton hien co la: ");
		String string = "";
		String temp = " ==> ";
		for (int k = 0; k < hc2.length; k++) {
			string += hc2[k] + 1;
			if (k < hc2.length - 1) {
				string += temp;
			}
		}
		System.out.println(string + temp + (hc2[0] + 1));

	}
///////////////////========================Bai tap tuan 6==================================

	@Override
	// in chu trinh hamilton
	public void findAllCycleHamilton() {
		if (checkCycleHamilton() == true) {
			for (int i = 0; i < numVex; i++) {
				visited[i] = false;
			}
			path[0] = 0;
			visited[0] = true;
			Expand2(1);
		} else {
			System.out.println("Khong co chu trinh hamilton");
		}
	}

	private void Expand2(int i) {
		for (int j = 0; j < numVex; j++) {
			if (adj[path[i - 1]][j] != 0 && visited[j] == false) {
				path[i] = j;
				if (i < numVex - 1) {
					visited[j] = true;
					Expand2(i + 1);
					visited[j] = false;
				} else if (i == numVex - 1) {
					if (adj[path[i]][path[0]] != 0) {
						printCycle(path);
					}
				}
			}
		}

	}

	private void printCycle(int[] path) {
		String result = "";
		for (int i = 0; i < path.length; i++) {
			result += path[i] + 1 + "==>";
		}
		System.out.println(result + (path[0] + 1));
	}

	@Override
	// in chu trinh hamilton tu1 dinh
	public void findAllCycleHamilton(int v) {
		if (checkCycleHamilton() == true) {
			for (int i = 0; i < numVex; i++) {
				visited[i] = false;
			}
			int start = v - 1;
			path[0] = start;
			visited[start] = true;
			Expand2(1);
		} else {
			System.out.println("Khong co chu trinh hamilton");
		}
	}

	@Override
	// in duong di hamilton
	public void findAllPathHamilton() {
		if (checkPathEuler() == true) {
			int minVex = minDegree();
			for (int i = 0; i < numVex; i++) {
				visited[i] = false;
			}
			for (int i = 0; i < numVex; i++) {
				if (degreeVex(i) == minVex) {
					path[0] = i;
					visited[i] = true;
					expandPath(1);
				}
			}

		} else {
			System.out.println("Khong co duong di hamilton");
		}
	}

	public void expandPath(int i) {
		for (int j = 0; j < numVex; j++) {
			if (adj[path[i - 1]][j] != 0 && visited[j] == false) {
				path[i] = j;
				if (i < numVex - 1) {
					visited[j] = true;
					expandPath(i + 1);
					visited[j] = false;
				} else if (i == numVex - 1) {
					printPath(path);
				}
			}
		}
	}

	public void printPath(int[] path) {
		String result = "";
		for (int i = 0; i < path.length; i++) {
			result += path[i] + 1 + "==>";
		}
		String result1 = result.substring(0, result.length() - 3);
		System.out.println(result1);
	}

	private int minDegree() {
		int min = degreeVex(0);
		for (int i = 0; i < numVex; i++) {
			if (min > degreeVex(i)) {
				min = degreeVex(i);
			}
		}
		return min;
	}

	@Override
	// in duong di hamilton tu 1 dinh
	public void findAllPathHamilton(int k) {
		ArrayList<Integer> listMinVex = new ArrayList<>();
		int minVex = minDegree();
		for (int i = 0; i < numVex; i++) {
			if (degreeVex(i) == minVex) {
				listMinVex.add(i);
			}
		}
		if (degreeVex(k) == minVex) {
			for (int i = 0; i < numVex; i++) {
				visited[i] = false;
			}
			path[0] = k;
			visited[k] = true;
			expandPath(1);
		} else {
			k = listMinVex.get(0);
			path[0] = k;
			visited[k] = true;
			expandPath(1);
		}
	}

	// =========================Bai Tap Tuan7============================

	@Override
	public Graph DFSTree(int v) {
		Graph tree = new UnGraph(numVex);
		tree.adj = treeDFS(v);
		System.out.println("Duyet cay theo DFS:");
		return tree;
	}

	int[][] a = new int[numVex][numVex];

	private int[][] treeDFS(int v) {
		int s = v - 1;
		visited[s] = true;
		for (int i = 0; i < numVex; i++) {
			if (adj[s][i] != 0 && visited[i] == false) {
				visited[i] = true;
				a[s][i] = a[i][s] = 1;
				treeDFS(i + 1);
			}
		}
		return a;
	}

	@Override
	public Graph BFSTree(int anyVex) {
		// System.out.println("Dang duyet tu dinh "+(anyVex+1));
		Graph tree = new UnGraph(numVex);
		tree.adj = treeBFS(anyVex);
		System.out.println("Duyet cay theo BFS:");
		return tree;
	}

	int b[][] = new int[numVex][numVex];

	public int[][] treeBFS(int anyVex) {

		Queue<Integer> queue = new LinkedList<Integer>();

		queue.add(anyVex);

		visited[anyVex] = true;
		while (!queue.isEmpty()) {
			int v = queue.remove();
			for (int i = 0; i < adj.length; i++) {
				if (adj[v][i] != 0 && visited[i] == false) {
					b[v][i] = b[i][v] = 1;
					queue.add(i);
					visited[i] = true;
				}
			}
		}

		return b;

	}

/////////=====================Bai tap tuan 8=========================
	@Override
	public boolean hasCycle(int a, int b) {
		// TODO Auto-generated method stub
		visited = new boolean[numVex];

		for (int i = 0; i < numVex; i++) {
			visited[i] = false;
		}
		Queue<Integer> q = new LinkedList<Integer>();
		q.add(a);
		while (!q.isEmpty()) {
			int v = q.remove();
			for (int i = 0; i < numVex; i++) {
				if (adj[v][i] != 0 && visited[i] == false) {
					visited[i] = true;
					q.add(i);
					if (i == b) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/*
	 * Sap xep cac canh co trong so tang dan
	 */
	private List<Edges> sortWeight() {
		List<Edges> list = new ArrayList<Graph.Edges>();
		for (int i = 0; i < numVex; i++) {
			for (int j = 0; j < numVex; j++) {
				if (adj[i][j] != 0) {
					list.add(new Edges(i, j, adj[i][j]));
				}
			}
		}
		Collections.sort(list, new Comparator<Edges>() {

			@Override
			public int compare(Edges e1, Edges e2) {
				// TODO Auto-generated method stub
				return e1.weight - e2.weight;
			}
		});
		return list;

	}

	@Override
	public Graph Kruskal() {
		System.out.println("Duyet Kruskal");
		Graph tree = new UnGraph(numVex);
		int trongSo = 0;
		int numberofEdges = 0;
		while (numberofEdges < numVex - 1) {
			List<Edges> listEdgesSorted = sortWeight();
			for (Edges edges : listEdgesSorted) {
				if (tree.hasCycle(edges.src, edges.des) == false) {

					tree.addEdges(edges.src + 1, edges.des + 1, edges.weight);
					System.out.println(edges.src + "--->" + edges.des + " : " + edges.weight);
					numberofEdges++;
					trongSo += edges.weight;

				}
			}
		}

		tree.printGraph();

		System.out.println("Trong so cua do thi la: " + trongSo);
		return tree;
	}

	@Override
	public int[][] Prim(int index) {

		int tree[][] = new int[numVex][numVex];
		int totalWeight = 0;
		List<Edges> listEdge = new ArrayList<Graph.Edges>();
		int visit[][] = new int[numVex][numVex];
		int countEdgeAdd = 0;

		visit[index][index] = 1;
		while (countEdgeAdd <= numVex - 1) {
			int i = 0;
			for (i = 0; i < visit.length; i++) {
				if (adj[index][i] > 0 && (visit[index][i] != 1 || visit[i][index] != 1)) {
					listEdge.add(new Edges(index, i, adj[index][i]));
					visit[i][index] = 1;
					visit[index][i] = 1;
				}
				if (i == visit.length - 1) {
					listEdge = sort(listEdge);
					Edges ed = listEdge.remove(0);
					while (checkHasCycle(ed.getSrc(), ed.getDes(), tree)) {
						ed = listEdge.remove(0);
						System.out.println(ed.src + "--->" + ed.des + " : " + ed.weight);
					}
					index = ed.getDes();
					tree[ed.getSrc()][ed.getDes()] = ed.getWeight();
					tree[ed.getDes()][ed.getSrc()] = ed.getWeight();
					totalWeight += ed.getWeight();
					countEdgeAdd++;
					break;
				}
			}
			if (countEdgeAdd == numVex - 1)
				break;

		}
		System.out.println("Duyet Prim");
		printPrim(tree);
		System.out.println("Tong trong so la: " + totalWeight);

		return tree;
	}

	public List<Edges> sort(List<Edges> list) {
		Collections.sort(list, new Comparator<Edges>() {
			public int compare(Edges e1, Edges e2) {
				return e1.weight - e2.weight;
			}
		});
		return list;
	}

	public boolean checkHasCycle(int x, int y, int[][] tree) {
		int numberPoint = numVex;
		ArrayList<Integer> queue = new ArrayList<Integer>();
		boolean[] visit;
		visit = new boolean[numberPoint];
		visit[x] = true;
		queue.add(0, x);
		while (queue.size() > 0) {
			x = queue.get(queue.size() - 1);
			queue.remove(queue.size() - 1);
			if (x == y) {
				return true;
			}
			for (int j = 0; j < numberPoint; j++) {
				if (tree[x][j] > 0 && !visit[j]) {
					visit[j] = true;
					queue.add(0, j);
					
				} 
			}
		}
		return false;

	}

	public void printPrim(int[][] arr) {
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr.length; j++) {
				System.out.print(arr[i][j] + "\t ");

			}
			System.out.println();
		}
	}

}
