package Tuan6;

import java.util.ArrayList;

public abstract class Graph {
	public int numVex;
	public int[][] adj;
	public int[] path;
	public boolean[] visited;
	protected ArrayList<Integer> listVisit = new ArrayList<>();
	protected ArrayList<Integer> vexVisited = new ArrayList<>();

	public Graph(int v) {
		this.numVex = v;
		this.adj = new int[numVex][numVex];
		visited = new boolean[numVex];
		path = new int[numVex];
	}

	// Tuan 1
	// Them canh
	public abstract void addEdges(int src, int dest);

	// Xoa canh
	public abstract void delEdges(int src, int dest);

	// Bac cua 1 dinh
	public abstract int degreeVex(int anyVex);

	// Bac cua do thi
	public abstract int degreeGraph();

	// So canh
	public abstract int numEdgesGraph();

	// in ra do thi
	public abstract void printGraph();

	// Tuan 2

	// Kiem tra do thi co phai la do thi luong phan
	public abstract boolean isBipartiteGraph();

	// Kiem tra do thi con
	public abstract boolean isSubGraph(Graph g1, Graph g2);

	// Duyet DFS duyet theo chieu sau de quy
	public abstract void re_DFS(int start);

	// Kiem tra lien thong
	public abstract boolean isConnection();

	// Dem thanh phan lien thong
	public abstract int countConnection();

	// Tim thanh phan lien thong
	public abstract void findConnection();

	// Tuan 3

	// Kiem tra do thi co phai con cua do thi khac hay khong
	public abstract boolean isSubGraph2(Graph g1, Graph g2);

	// Duyet DFS = STACK
	public abstract void DFS(int start);

	// Duyet BFS = queue
	public abstract void BFS(int start);

	// Tim duong di dai nhat = dfs
	public abstract void findPathLong(int a, int b);

	// Tim duong di ngan nhat = bfs
	public abstract void findPathShortest(int a, int b);

	// Tuan 4

	// Dung DFS
	public abstract void findWay(int a, int b);

	// Dung BFS
	public abstract void findWayShortest(int a, int b);

	// Check chu trinh euler
	public abstract boolean checkCycleEuler();

	// Check duong di Euler
	public abstract boolean checkPathEuler();

	// In ra chu trinh Euler
	public abstract void findCycleEuler(int start);

	// In ra duong di Euler
	public abstract void findPathEuler(int start);

	// Tuan 5

	// kiem tra chu trinh hamilton
	public abstract boolean checkCycleHamilton();

	// kiem tra duong di hamilton
	public abstract boolean checkPathHamilton();

	// In ra 1 chu trinh hamilton
	public abstract void findCycleHamilton();

///////////////////========================Bai tap tuan 6==================================

	// In ra 1 chu trinh hamilton
	public abstract void findAllCycleHamilton();

	public abstract void findAllCycleHamilton(int v);

	public abstract void findAllPathHamilton();

	public abstract void findAllPathHamilton(int v);

	// Lop canh
	protected class Edges {
		int src, des;

		protected Edges(int a, int b) {
			super();
			this.src = a;
			this.des = b;
		}

		@Override
		public String toString() {
			return "Edges: " + "(" + src + "," + des + ")";
		}

	}
}